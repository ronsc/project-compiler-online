package routes

import (
	"net/http"

	"go-code/project-compiler-online/src/controllers"
	// "../controllers"
	// "cplonline/src/controllers"
)

func init() {
	http.HandleFunc("/member/checklogin", controllers.Member_CheckLogin)
	http.HandleFunc("/member/checkuser", controllers.Member_CheckUser)
	http.HandleFunc("/member/finduser", controllers.Member_FindUser)
	http.HandleFunc("/member/registration", controllers.Member_Registration)
	http.HandleFunc("/member/listall", controllers.Member_ListAll)
	http.HandleFunc("/member/updatedata", controllers.Member_UpdateData)
	http.HandleFunc("/member/uploadimage", controllers.Member_UploadImage)
	http.HandleFunc("/member/changepassword", controllers.Member_ChangePassword)
	http.HandleFunc("/member/delete", controllers.Member_Delete)
	http.HandleFunc("/member/changelevel", controllers.Member_ChangeLevel)

	http.HandleFunc("/team/registration", controllers.Team_Registration)
	http.HandleFunc("/team/listall", controllers.Team_ListAll)
	http.HandleFunc("/team/generatepassword", controllers.Team_GeneratePassWord)
	http.HandleFunc("/team/checklogin", controllers.Team_CheckLogin)
	http.HandleFunc("/team/match/list", controllers.Team_ListByMatchId)
	http.HandleFunc("/team/match/listall", controllers.Team_MatchListAll)
	http.HandleFunc("/team/checkreg", controllers.Team_CheckRegistration)
	http.HandleFunc("/team/acceptmember", controllers.Team_AcceptMember)

	http.HandleFunc("/task/listall", controllers.Task_ListAll)
	http.HandleFunc("/task/list/user", controllers.Task_ListByUser)
	http.HandleFunc("/task/list/level", controllers.Task_FindLevel)
	http.HandleFunc("/task/list/code", controllers.Task_FindCode)
	http.HandleFunc("/task/list/mode", controllers.Task_FindMode)
	http.HandleFunc("/task/uploadio", controllers.Task_UploadIO)
	http.HandleFunc("/task/getnewcode", controllers.Task_GetNewCode)
	http.HandleFunc("/task/run", controllers.Task_Run)
	http.HandleFunc("/task/submit", controllers.Task_Submit)
	http.HandleFunc("/task/delete", controllers.Task_Delete)
	http.HandleFunc("/task/list/code/edit", controllers.Task_FindEditCode)
	http.HandleFunc("/task/update", controllers.Task_Update)

	http.HandleFunc("/tasksubmit/listall", controllers.TaskSubmit_ListAll)
	http.HandleFunc("/tasksubmit/list/match", controllers.TaskSubmit_ListMatch)
	http.HandleFunc("/tasksubmit/list/train", controllers.TaskSubmit_ListTrain)
	http.HandleFunc("/tasksubmit/list/match/code", controllers.TaskSubmit_MathchFindByCode)
	http.HandleFunc("/tasksubmit/list/train/username", controllers.TaskSubmit_ListTrainByUserName)

	http.HandleFunc("/score/list", controllers.Score_List)
	http.HandleFunc("/score/update", controllers.Score_Update)

	http.HandleFunc("/match/list", controllers.Match_ListByMatchId)
	http.HandleFunc("/match/task", controllers.Match_ListByTaskCode)
	http.HandleFunc("/match/listall", controllers.Match_ListAll)
	http.HandleFunc("/match/gettime", controllers.Match_GetTime)
	http.HandleFunc("/match/setup", controllers.Match_Setup)
	http.HandleFunc("/match/save", controllers.Match_Save)
	http.HandleFunc("/match/public", controllers.Match_Public)
	http.HandleFunc("/match/delete", controllers.Match_Delete)

	http.HandleFunc("/notification/save", controllers.Notification_Save)
	http.HandleFunc("/notification/listall", controllers.Notification_ListAll)
	http.HandleFunc("/notification/getcount", controllers.Notification_GetCount)

	http.HandleFunc("/scoreboard/save", controllers.ScoreBoard_Save)
	http.HandleFunc("/scoreboard/listall", controllers.ScoreBoard_ListAll)
	http.HandleFunc("/scoreboard/team/list", controllers.ScoreBoard_ListByTeam)
	http.HandleFunc("/scoreboard/team/listpass", controllers.ScoreBoard_ListByPass)

	http.HandleFunc("/ranking/listall", controllers.Ranking_ListAll)
	http.HandleFunc("/ranking/list", controllers.Ranking_ListByUserName)
	http.HandleFunc("/ranking/listall/test", controllers.Test_Ranking_ListAll)
}
