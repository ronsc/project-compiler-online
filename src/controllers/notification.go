package controllers

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
	"strconv"
	// "strings"
	"time"
	// "math"

	// "fmt"
	// "log"

	"go-code/project-compiler-online/src/models"
	"go-code/project-compiler-online/src/render"
	// "../models"
	// "../render"
	// "cplonline/src/models"
	// "cplonline/src/render"
)

func Notification_Save(w http.ResponseWriter, r *http.Request) {
	r.ParseForm()

	result := make(map[string]interface{})
	bytes, _ := ioutil.ReadAll(r.Body)
	data := &models.Notification{}
	data.Date = time.Now()
	json.Unmarshal(bytes, &data)

	matchid, _ := strconv.Atoi(r.URL.Query().Get("matchid"))
	match, _ := models.Match_ListByMatchId(matchid)
	match.Notification = append(match.Notification, *data)
	_, err := models.Match_SettingsUpdate(&match)
	result["status"] = true
	if err != nil {
		result["status"] = false
	}

	render.RenderJSON(w, result)
}

func Notification_ListAll(w http.ResponseWriter, r *http.Request) {
	matchid, _ := strconv.Atoi(r.URL.Query().Get("matchid"))

	m, _ := models.Match_ListByMatchId(matchid)
	data := m.Notification

	render.RenderJSON(w, data)
}

func Notification_GetCount(w http.ResponseWriter, r *http.Request) {
	matchid, _ := strconv.Atoi(r.URL.Query().Get("matchid"))

	m, _ := models.Match_ListByMatchId(matchid)

	render.RenderTEXT(w, strconv.Itoa(len(m.Notification)))
}
