package controllers

import (
	"encoding/json"
	// "fmt"
	// "io"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"time"
	// "strings"
	// "time"

	"go-code/project-compiler-online/src/models"
	"go-code/project-compiler-online/src/render"
	// "../models"
	// "../render"
	// "cplonline/src/models"
	// "cplonline/src/render"
)

func Member_Registration(w http.ResponseWriter, r *http.Request) {
	r.ParseForm()
	bytes, _ := ioutil.ReadAll(r.Body)

	data := &models.Member{}
	json.Unmarshal(bytes, &data)

	data.Date = time.Now()
	models.Member_Save(data)

	src := "www/images/profile.jpg"
	dest := "www/uploads/" + data.UserName + "/profile/"

	// create dir
	os.MkdirAll(dest, 0644)

	err := render.CopyFile(src, dest+"profile.jpg")
	if err != nil {
		render.RenderTEXT(w, err.Error())
	} else {
		render.RenderTEXT(w, "member save success")
	}
}

func Member_CheckUser(w http.ResponseWriter, r *http.Request) {
	q := r.URL.Query()
	user := string(q.Get("us"))

	log.Println(user)
	result := models.Member_CheckUser(user)
	if result != nil {
		render.RenderTEXT(w, "false")
	} else {
		render.RenderTEXT(w, "true")
	}
}

func Member_FindUser(w http.ResponseWriter, r *http.Request) {
	q := r.URL.Query()
	user := string(q.Get("us"))

	result := models.Member_CheckUser(user)

	render.RenderJSON(w, result)
}

func Member_ListAll(w http.ResponseWriter, r *http.Request) {
	members, _ := models.Member_ListAll()
	response := map[string]interface{}{
		"data": members,
	}

	render.RenderJSON(w, response)
}

func Member_CheckLogin(w http.ResponseWriter, r *http.Request) {
	q := r.URL.Query()
	user := string(q.Get("us"))
	pass := string(q.Get("pw"))

	m := models.Member_CheckLogin(user, pass)

	render.RenderJSON(w, m)
}

func Member_UpdateData(w http.ResponseWriter, r *http.Request) {
	r.ParseForm()

	bytes, _ := ioutil.ReadAll(r.Body)

	data := &models.Member{}
	json.Unmarshal(bytes, &data)

	ret, _ := models.Member_Update(data)

	render.RenderJSON(w, ret)
}

func Member_Delete(w http.ResponseWriter, r *http.Request) {
	q := r.URL.Query()
	user := string(q.Get("user"))

	mapData := make(map[string]interface{})
	err := models.Member_Delete(user)

	if err == nil {
		mapData["status"] = true
		mapData["message"] = "ลบผู้ใช้งาน " + user + " สำเร็จ"
	} else {
		mapData["status"] = false
		mapData["message"] = "ลบผู้ใช้งาน " + user + " ไม่สำเร็จ<br />เนื่องจาก : " + err.Error()
	}

	render.RenderJSON(w, mapData)
}

func Member_ChangePassword(w http.ResponseWriter, r *http.Request) {
	q := r.URL.Query()
	user := string(q.Get("user"))
	old_pw := string(q.Get("old_pw"))
	new_pw := string(q.Get("new_pw"))

	member := models.Member_ListByUser(user)

	mapData := make(map[string]interface{})
	if old_pw == member.PassWord {
		member.PassWord = new_pw
		models.Member_Update(&member)

		mapData["status"] = true
		mapData["message"] = "เปลี่ยนรหัสผ่าน สำเร็จ"
	} else {
		mapData["status"] = false
		mapData["message"] = "รหัสผ่านเดิมไม่ถูกต้อง"
	}

	render.RenderJSON(w, mapData)
}

func Member_ChangeLevel(w http.ResponseWriter, r *http.Request) {
	q := r.URL.Query()
	user := string(q.Get("user"))

	member := models.Member_ListByUser(user)
	if member.Level == "ADMIN" {
		member.Level = "USER"
	} else {
		member.Level = "ADMIN"
	}
	_, err := models.Member_Update(&member)

	mapData := make(map[string]interface{})
	if err == nil {
		mapData["status"] = true
		mapData["message"] = user + " เปลี่ยนระดับผู้ใช้งาน สำเร็จ"
	} else {
		mapData["status"] = false
		mapData["message"] = user + " เปลี่ยนระดับผู้ใช้งาน ไม่สำเร็จ"
	}

	render.RenderJSON(w, mapData)
}

func Member_UploadImage(w http.ResponseWriter, r *http.Request) {
	mapData := map[string]string{
		"formFile": "img",
		"category": "profile",
		"name":     "profile",
		"dir":      "profile",
	}
	render.RenderUPLOAD(w, r, mapData)
}
