package controllers

import (
	"net/http"
	// "log"
	// "fmt"
	// "strconv"
	"math"
	"math/rand"

	"go-code/project-compiler-online/src/models"
	"go-code/project-compiler-online/src/render"
	// "../models"
	// "../render"
	// "cplonline/src/models"
	// "cplonline/src/render"
)

// data for views [/]
func Ranking_ListAll(w http.ResponseWriter, r *http.Request) {
	_UpdatePointAll()

	m := models.Ranking_ListAll()

	render.RenderJSON(w, m)
}

func _UpdatePointAll() {
	chk, _ := models.Score_List()
	if chk == nil {
		Score_Init()
	}
	_, score := models.Score_List()
	div_per_level := score.ScorePerLevel

	_, members := models.Member_ListAll()
	for _, v := range members {
		UserName := v.UserName

		rk := models.Ranking_ListByUserName(UserName)
		point := _CalculatePoint(UserName)

		rk.UserName = UserName
		rk.CodeName = v.CodeName
		rk.Point = point
		rk.Level = int(math.Floor(rk.Point / float64(div_per_level)))
		rk.Percen = (rk.Point - float64(rk.Level*div_per_level))
		rk.Percen /= float64(div_per_level)
		rk.Percen *= float64(100)

		if rk.Id.Hex() == "" {
			models.Ranking_Save(&rk)
		} else {
			models.Ranking_Update(&rk)
		}
	}
}

func Test_Ranking_ListAll(w http.ResponseWriter, r *http.Request) {
	div_per_level := 50
	var x [50]models.Ranking

	dictionary := "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"
	for i := 0; i < len(x); i++ {
		var bytes = make([]byte, rand.Intn(5)+5)
		for k, _ := range bytes {
			bytes[k] = dictionary[rand.Intn(len(dictionary))]
		}
		x[i].UserName = string(bytes)
		x[i].Point = rand.Float64() * float64(rand.Intn(1000))
		x[i].Level = int(math.Floor(x[i].Point / float64(div_per_level)))
		x[i].Percen = (x[i].Point - float64(x[i].Level*div_per_level))
		x[i].Percen /= float64(div_per_level)
		x[i].Percen *= float64(100)
	}
	render.RenderJSON(w, x)
}

func _CalculatePoint(username string) (point float64) {
	result := make(map[string]map[string]int)
	status := 0
	tasksubmitTrains, _, _ := models.TaskSubmit_TrainFindByUserName(username)

	for _, v := range tasksubmitTrains {
		if v.Status == "pass" {
			status = 1
		}
		count := models.TaskSubmit_CountCodeNotPassTrainMode(v.UserName, v.Code)
		result[v.Code] = map[string]int{
			"count":  count,
			"status": status,
		}
	}

	_, score := models.Score_List()
	point = 0
	for k, v := range result {
		level := string(k[0])
		var (
			score_per_level int
			minus_per_level float64
		)

		if v["status"] == 1 {
			if level == "E" {
				score_per_level = score.Easy
				minus_per_level = score.EasyMinus
			} else if level == "M" {
				score_per_level = score.Medium
				minus_per_level = score.MediumMinus
			} else if level == "H" {
				score_per_level = score.Hard
				minus_per_level = score.HardMinus
			}

			if v["count"] < score.Times {
				point += (float64(score_per_level) - (minus_per_level * float64(v["count"])))
			} else {
				point += (float64(score_per_level) / (minus_per_level * float64(score.Min)))
			}
		}
	}

	return point
}

func Ranking_ListByUserName(w http.ResponseWriter, r *http.Request) {
	params := r.URL.Query()

	username := params.Get("UserName")
	m := models.Ranking_ListByUserName(username)

	render.RenderJSON(w, m)
}
