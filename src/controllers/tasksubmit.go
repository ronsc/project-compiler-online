package controllers

import (
	// "log"
	// "fmt"
	"net/http"
	"strconv"
	"strings"

	"go-code/project-compiler-online/src/models"
	"go-code/project-compiler-online/src/render"
	// "../models"
	// "../render"
	// "cplonline/src/models"
	// "cplonline/src/render"
)

func TaskSubmit_ListAll(w http.ResponseWriter, r *http.Request) {
	m, _ := models.TaskSubmit_ListAll()

	render.RenderJSON(w, m)
}

func TaskSubmit_ListMatch(w http.ResponseWriter, r *http.Request) {
	m := models.TaskSubmit_FindByMode("MATCH")

	render.RenderJSON(w, m)
}

func TaskSubmit_ListTrain(w http.ResponseWriter, r *http.Request) {
	myMap := make(map[string]interface{})
	BY := r.URL.Query().Get("By")
	var m []models.TaskSubmit

	if BY == "me" {
		user := r.URL.Query().Get("user")
		m = models.TaskSubmit_FindTrainByUser(user)
	} else if BY == "mecode" {
		code := r.URL.Query().Get("code")
		user := r.URL.Query().Get("user")
		m = models.TaskSubmit_FindTrainByCode(code, user)
	} else if BY == "code" {
		code := r.URL.Query().Get("code")
		m = models.TaskSubmit_FindTrainByCode(code, "")
	} else {
		m = models.TaskSubmit_FindTrainByUser("")
	}

	if len(m) != 0 {
		myMap["data"] = m
	} else {
		myMap["data"] = make([]string, 0)
	}

	render.RenderJSON(w, myMap)
}

// data for views [member/profile]
func TaskSubmit_ListTrainByUserName(w http.ResponseWriter, r *http.Request) {
	username := r.URL.Query().Get("USERNAME")
	all, pass, _ := models.TaskSubmit_TrainFindByUserName(username)

	m := make(map[string]interface{})

	count_easy := 0
	count_medium := 0
	count_hard := 0

	for _, v := range pass {
		if strings.Contains(v.Code, "E") {
			count_easy++
		} else if strings.Contains(v.Code, "M") {
			count_medium++
		} else if strings.Contains(v.Code, "H") {
			count_hard++
		}
	}

	mapData := make(map[string]string)
	for _, v := range all {
		mapData[v.Code] = v.Status
	}

	var (
		passArray    []string
		notpassArray []string
	)
	for k, v := range mapData {
		if v == "pass" {
			passArray = append(passArray, k)
		} else {
			notpassArray = append(notpassArray, k)
		}
	}

	if len(passArray) == 0 {
		passArray = append(passArray, "-")
	}
	if len(notpassArray) == 0 {
		notpassArray = append(notpassArray, "-")
	}

	m["pass"] = passArray
	m["notpass"] = notpassArray
	m["statistic"] = map[string]int{
		"count_easy":   count_easy,
		"count_medium": count_medium,
		"count_hard":   count_hard,
	}

	_UpdatePointAll()
	m["ranking"] = models.Ranking_ListByUserName(username)

	render.RenderJSON(w, m)
}

func TaskSubmit_MathchFindByCode(w http.ResponseWriter, r *http.Request) {
	code := r.URL.Query().Get("code")
	status := r.URL.Query().Get("status")
	matchid, _ := strconv.Atoi(r.URL.Query().Get("matchid"))

	var m []models.TaskSubmit
	if status != "pass" {
		m = models.TaskSubmit_MathchFindByCode(code, matchid)
	} else {
		m = models.TaskSubmit_MathchFindByCodePass(code, matchid)
	}

	render.RenderJSON(w, m)
}
