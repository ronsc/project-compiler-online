package controllers

import (
	// "log"
	"encoding/json"
	// "fmt"
	"io/ioutil"
	"net/http"

	"go-code/project-compiler-online/src/models"
	"go-code/project-compiler-online/src/render"
	// "../models"
	// "../render"
	// "cplonline/src/models"
	// "cplonline/src/render"
)

func Score_List(w http.ResponseWriter, r *http.Request) {
	result, _ := models.Score_List()
	if result == nil {
		Score_Init()
		result, _ = models.Score_List()
	}

	render.RenderJSON(w, result)
}

func Score_Update(w http.ResponseWriter, r *http.Request) {
	r.ParseForm()

	bytes, _ := ioutil.ReadAll(r.Body)

	data := &models.Score{}
	json.Unmarshal(bytes, &data)

	ret := models.Score_Update(data)

	render.RenderJSON(w, ret)
}

func Score_Init() {
	s := &models.Score{}
	s.Easy = 0
	s.Medium = 0
	s.Hard = 0
	s.Min = 0
	s.EasyMinus = float64(0)
	s.MediumMinus = float64(0)
	s.HardMinus = float64(0)
	s.Times = 3
	s.ScorePerLevel = 10
	models.Score_Save(s)
}
