package controllers

import (
	"encoding/json"
	"fmt"
	"log"
	// "net/http"
	"bytes"
	"io/ioutil"
	"net/http"
	"os"
	"os/exec"
	"regexp"
	"strconv"
	"strings"
	"time"

	"go-code/project-compiler-online/src/models"
	"go-code/project-compiler-online/src/render"
	// "../models"
	// "../render"
	// "cplonline/src/models"
	// "cplonline/src/render"

	"gopkg.in/mgo.v2/bson"
)

func Task_ListAll(w http.ResponseWriter, r *http.Request) {
	tasks := models.Task_ListAll()
	if tasks == nil {
		tasks = make([]interface{}, 0)
	}
	response := map[string]interface{}{
		"data": tasks,
	}

	render.RenderJSON(w, response)
}

func Task_Delete(w http.ResponseWriter, r *http.Request) {
	code := r.URL.Query().Get("code")
	err := models.Task_Delete(code)

	result := make(map[string]string)

	if err != nil {
		result["status"] = "error"
		result["code"] = code
		result["message"] = err.Error()
		render.RenderJSON(w, result)
	} else {
		result["status"] = "success"
		result["code"] = code
		result["message"] = "delete success"
		render.RenderJSON(w, result)
	}
}

func Task_UploadIO(w http.ResponseWriter, r *http.Request) {
	p := &models.Task{}
	p.Id = bson.NewObjectId()
	p.Code = r.FormValue("code")
	p.Level = r.FormValue("level")
	p.Title = r.FormValue("title")
	p.Content = r.FormValue("content")
	p.Help = r.FormValue("help")
	p.SendTotal = 0
	p.PassTotal = 0
	p.Mode = r.FormValue("mode")
	p.Date = time.Now()

	if r.URL.Query().Get("mode") == "TRAIN" {
		models.Task_Save(p)

		mapInput := map[string]string{
			"formFile": "inputFile",
			"category": "task",
			"name":     "input",
			"dir":      p.Code,
		}
		mapOutput := map[string]string{
			"formFile": "outputFile",
			"category": "task",
			"name":     "output",
			"dir":      p.Code,
		}

		render.RenderUPLOAD(w, r, mapInput)
		render.RenderUPLOAD(w, r, mapOutput)
	} else {
		matchid, _ := strconv.Atoi(r.URL.Query().Get("matchid"))
		match, _ := models.Match_ListByMatchId(matchid)
		match.Task = append(match.Task, *p)
		models.Match_SettingsUpdate(&match)

		mapInput := map[string]string{
			"formFile": "inputFile",
			"category": "task",
			"name":     "input",
			"dir":      "MATCH/" + r.URL.Query().Get("matchid") + "/" + p.Code,
		}
		mapOutput := map[string]string{
			"formFile": "outputFile",
			"category": "task",
			"name":     "output",
			"dir":      "MATCH/" + r.URL.Query().Get("matchid") + "/" + p.Code,
		}

		render.RenderUPLOAD(w, r, mapInput)
		render.RenderUPLOAD(w, r, mapOutput)
	}

	log.Print("Task Add Success")
}

func Task_Update(w http.ResponseWriter, r *http.Request) {
	r.ParseForm()

	bytes, _ := ioutil.ReadAll(r.Body)

	task := &models.Task{}
	json.Unmarshal(bytes, &task)
	// update task data in db
	err := models.Task_Update(task)
	if err != nil {
		log.Println(err)
		return
	}

	type InputOutput struct {
		Input  string
		Output string
	}
	var data InputOutput
	json.Unmarshal(bytes, &data)
	var (
		input  = data.Input
		output = data.Output
	)
	// update data to files
	dir := "uploads/task/" + task.Code
	os.MkdirAll(dir, 0644)
	os.Create(dir + "/input")
	os.Create(dir + "/output")
	ioutil.WriteFile(dir+"/input", []byte(input), 0644)
	ioutil.WriteFile(dir+"/output", []byte(output), 0644)

	mapData := map[string]string{
		"status":  "success",
		"message": task.Code + " : update success",
	}

	render.RenderJSON(w, mapData)
}

func Task_GetNewCode(w http.ResponseWriter, r *http.Request) {
	params := r.URL.Query()
	var code string

	if params.Get("mode") == "TRAIN" {
		total := models.Task_CountLevel(params.Get("level"))
		level := params.Get("level")

		if level == "EASY" {
			if (total + 1) < 10 {
				code = "E00" + strconv.Itoa(total+1)
			} else {
				code = "E0" + strconv.Itoa(total+1)
			}
		} else if level == "MEDIUM" {
			if (total + 1) < 10 {
				code = "M10" + strconv.Itoa(total+1)
			} else {
				code = "M1" + strconv.Itoa(total+1)
			}
		} else {
			if (total + 1) < 10 {
				code = "H20" + strconv.Itoa(total+1)
			} else {
				code = "H2" + strconv.Itoa(total+1)
			}
		}
	} else {
		matchid, _ := strconv.Atoi(params.Get("matchid"))
		t, _ := models.Match_ListByMatchId(matchid)
		count := len(t.Task)
		ch := []string{"A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"}

		code = ch[count]
	}

	render.RenderTEXT(w, code)
}

func Task_FindLevel(w http.ResponseWriter, r *http.Request) {
	params := r.URL.Query()

	result, _ := models.Task_FindByLevel(params.Get("level"))

	render.RenderJSON(w, result)
}

func Task_FindCode(w http.ResponseWriter, r *http.Request) {
	code := r.URL.Query().Get("code")

	result, _ := models.Task_FindByCode(code)

	render.RenderJSON(w, result)
}

func Task_FindEditCode(w http.ResponseWriter, r *http.Request) {
	code := r.URL.Query().Get("code")

	result, _ := models.Task_FindByCode(code)

	// read input and output file
	fileInput, _ := ioutil.ReadFile("uploads/task/" + code + "/input")
	fileOutput, _ := ioutil.ReadFile("uploads/task/" + code + "/output")

	mapData := map[string]interface{}{
		"data":   result,
		"input":  string(fileInput),
		"output": string(fileOutput),
	}

	render.RenderJSON(w, mapData)
}

func Task_FindMode(w http.ResponseWriter, r *http.Request) {
	params := r.URL.Query()
	mode := params.Get("mode")
	matchid, _ := strconv.Atoi(params.Get("matchid"))

	_, result := models.Task_FindAllByMode(mode)
	if mode == "MATCH" {
		m, _ := models.Match_ListByMatchId(matchid)
		result = m.Task
	}

	render.RenderJSON(w, result)
}

func Task_ListByUser(w http.ResponseWriter, r *http.Request) {
	params := r.URL.Query()
	USER := params.Get("user")

	result := map[string]interface{}{
		"user": USER,
		"easy": map[string]interface{}{
			"tasks": _GetTasks(USER, "EASY"),
		},
		"medium": map[string]interface{}{
			"tasks": _GetTasks(USER, "MEDIUM"),
		},
		"hard": map[string]interface{}{
			"tasks": _GetTasks(USER, "HARD"),
		},
	}

	render.RenderJSON(w, result)
}

func _GetTasks(user string, level string) interface{} {
	tasks := models.Task_TrainFindByLevel(level)
	m := make([]map[string]interface{}, len(tasks))
	for i, v := range tasks {
		status := "null"
		tasksubmit := models.TaskSubmit_TrainFindByUserNameAndCode(user, v.Code)
		if tasksubmit != nil {
			for _, vv := range tasksubmit {
				status = vv.Status
				if vv.Status == "pass" {
					break
				}
			}
		}

		task := map[string]interface{}{
			"code":      v.Code,
			"title":     v.Title,
			"sendtotal": v.SendTotal,
			"passtotal": v.PassTotal,
			"status":    status,
		}

		m[i] = task
	}
	return m
}

type request struct {
	lang  string
	dir   string
	input string
	w     http.ResponseWriter
	r     *http.Request
	done  chan bool
}

var que = make(chan *request, 200)

func Task_Run(w http.ResponseWriter, r *http.Request) {
	detail := r.URL.Query()

	// Status for Compile & Run Code
	done := make(chan bool)

	datas, _ := ioutil.ReadAll(r.Body)

	// create dir
	dir := "queue/" + detail.Get("code") + "/" + detail.Get("username") + "/run"
	os.MkdirAll(dir, 0644)

	// Write File to OS
	lang := detail.Get("lang")
	if lang == "c_cpp" {
		ioutil.WriteFile(dir+"/Main.c", datas, 0644)
	} else if lang == "java" {
		ioutil.WriteFile(dir+"/Main.java", datas, 0644)
	} else if lang == "golang" {
		ioutil.WriteFile(dir+"/Main.go", datas, 0644)
	}

	// Queue of Compile & Run Code
	go func() {
		for {
			// wait
			fmt.Println("Complier Wait...")
			queue := <-que

			// compile
			var cmd *exec.Cmd
			lang := detail.Get("lang")
			if lang == "c_cpp" {
				cmd = exec.Command("gcc", "main.c", "-o", "main.exe")
			} else if lang == "java" {
				cmd = exec.Command("javac", "-d", ".", "Main.java")
			} else if lang == "golang" {
				cmd = exec.Command("go", "build", "Main.go")
			}
			cmd.Dir = queue.dir
			result, compileError := cmd.CombinedOutput()

			if compileError != nil {
				render.RenderHTML(queue.w, result)
				ioutil.WriteFile(queue.dir+"/Output", result, 0644)
				// log result
				log.Println("RUN :", detail.Get("code"), "Compilation error", detail.Get("lang"))
			} else {
				// run
				var runCmd *exec.Cmd
				if lang == "c_cpp" {
					runCmd = exec.Command("./main")
				} else if lang == "java" {
					runCmd = exec.Command("java", "-cp", ".", "Main")
				} else if lang == "golang" {
					runCmd = exec.Command("./Main")
				}
				runCmd.Stdin = strings.NewReader(queue.input)
				runCmd.Dir = queue.dir
				runResult, _ := runCmd.Output()

				render.RenderHTML(queue.w, runResult)
				ioutil.WriteFile(queue.dir+"/Output", runResult, 0644)
			}

			queue.done <- true
		}
	}()

	// que <- struct request
	que <- &request{detail.Get("lang"), dir, detail.Get("input"), w, r, done}
	// wait until the end
	<-done
}

func Task_Submit(w http.ResponseWriter, r *http.Request) {
	// params
	detail := r.URL.Query()

	// Status for Compile & Run Code
	done := make(chan bool)

	// read code from request
	datas, _ := ioutil.ReadAll(r.Body)

	// create dir
	dir := "queue/" + detail.Get("code") + "/" + detail.Get("username")
	if detail.Get("mode") == "MATCH" {
		dir = fmt.Sprintf("queue/MATCH/%s/%s/%s", detail.Get("matchid"), detail.Get("teamid"), detail.Get("code"))
	}
	os.MkdirAll(dir, 0644)

	// Write File to OS
	lang := detail.Get("lang")
	if lang == "c_cpp" {
		ioutil.WriteFile(dir+"/Main.c", datas, 0644)
	} else if lang == "java" {
		ioutil.WriteFile(dir+"/Main.java", datas, 0644)
	} else if lang == "golang" {
		ioutil.WriteFile(dir+"/Main.go", datas, 0644)
	}

	// Queue of Compile & Run Code
	go func() {
		for {
			// wait
			log.Println("Complier Wait...")
			queue := <-que

			// read input and output file
			fileInput, _ := ioutil.ReadFile("uploads/task/" + detail.Get("code") + "/input")
			fileOutput, _ := ioutil.ReadFile("uploads/task/" + detail.Get("code") + "/output")
			if detail.Get("mode") == "MATCH" {
				checkDir := fmt.Sprintf("uploads/task/MATCH/%s/%s", detail.Get("matchid"), detail.Get("code"))
				fileInput, _ = ioutil.ReadFile(checkDir + "/input")
				fileOutput, _ = ioutil.ReadFile(checkDir + "/output")
			}

			// split input and output from file
			s := regexp.MustCompile("\\[case\\-\\d\\]")
			inputs := s.Split(string(fileInput), -1)
			outputs := s.Split(string(fileOutput), -1)

			// create dir result
			dirResult := "uploads/task/" + detail.Get("code") + "/submit/" + detail.Get("username")
			if detail.Get("mode") == "MATCH" {
				dirResult = fmt.Sprintf("uploads/task/MATCH/%s/%s/submit/%s", detail.Get("matchid"), detail.Get("code"), detail.Get("username"))
			}
			os.MkdirAll(dirResult, 0644)
			os.Create(dirResult + "/output")

			// open files r and w
			file, err := os.OpenFile(dirResult+"/output", os.O_APPEND|os.O_WRONLY, 0644)
			if err != nil {
				log.Println(err)
			}
			defer file.Close()

			// compile
			var (
				testCase string
				status   string

				cmd    *exec.Cmd
				runCmd *exec.Cmd
			)
			lang := detail.Get("lang")
			if lang == "c_cpp" {
				cmd = exec.Command("gcc", "main.c", "-o", "main.exe")
			} else if lang == "java" {
				cmd = exec.Command("javac", "-d", ".", "Main.java")
			} else if lang == "golang" {
				cmd = exec.Command("go", "build", "Main.go")
			}
			cmd.Dir = queue.dir
			result, compileError := cmd.CombinedOutput()

			// run and writefile
			if compileError != nil {
				ioutil.WriteFile(dirResult+"/output", result, 0644)
				testCase = "Compilation error"
				status = "error"
			} else {
				for i, input := range inputs {
					if strings.TrimSpace(input) != "" {
						// run
						input = strings.TrimSpace(input)
						if lang == "c_cpp" {
							runCmd = exec.Command("./main")
						} else if lang == "java" {
							runCmd = exec.Command("java", "-cp", ".", "Main")
						} else if lang == "golang" {
							runCmd = exec.Command("./Main")
						}
						runCmd.Stdin = strings.NewReader(input)
						runCmd.Dir = queue.dir
						runResult, _ := runCmd.Output()

						// get result by case
						output := []byte(strings.TrimSpace(outputs[i]))
						if bytes.Equal(output, runResult) {
							testCase += "P"
						} else {
							testCase += "-"
						}

						// write output to file by case
						strResult := "[case-" + strconv.Itoa(i) + "]\n" + string(runResult) + "\n"
						if _, err = file.WriteString(strResult); err != nil {
							log.Println(err)
						}
					}
				}
				// check status from testcase
				if strings.Index(testCase, "-") == -1 {
					status = "pass"
				} else {
					status = "notpass"
				}
			}

			// save submit to db
			p := &models.TaskSubmit{}
			p.Code = detail.Get("code")
			p.Date = time.Now()
			p.UserName = detail.Get("username")
			p.Lang = detail.Get("lang")
			p.Result = testCase
			p.Status = status
			p.Mode = detail.Get("mode")
			if p.Mode == "MATCH" {
				p.TeamId = detail.Get("teamid")

				matchid, _ := strconv.Atoi(detail.Get("matchid"))
				p.MatchId = matchid

				// update [sendtotal passtotal] on task
				m, _ := models.Match_ListByMatchId(matchid)
				tasks := m.Task
				for index, v := range tasks {
					if p.Code == v.Code {
						v.SendTotal += 1
						if p.Status == "pass" {
							v.PassTotal += 1
						}
						m.Task[index] = v
					}
				}
				// update db
				models.Match_SettingsUpdate(&m)
			} else {
				// update [sendtotal passtotal] on task
				_, task := models.Task_FindByCode(p.Code)
				task.SendTotal, task.PassTotal = models.TaskSubmit_GetTimeSendAndPass(p.Mode, p.Code)
				// update save
				models.Task_Update(&task)
			}

			// save
			models.TaskSubmit_Save(p)
			// log result
			log.Println(detail.Get("code"), testCase, detail.Get("lang"))

			// render result
			render.RenderTEXT(w, status)
			// end queue
			queue.done <- true
		}
	}()

	// que <- struct request
	que <- &request{detail.Get("lang"), dir, "", w, r, done}
	// wait until the end
	<-done
}
