package controllers

import (
	"encoding/json"
	// "fmt"
	"io/ioutil"
	// "log"
	"net/http"
	"sort"
	"strconv"
	"time"

	"go-code/project-compiler-online/src/models"
	"go-code/project-compiler-online/src/render"
	// "../models"
	// "../render"
	// "cplonline/src/models"
	// "cplonline/src/render"

	"gopkg.in/mgo.v2/bson"
)

func Team_Registration(w http.ResponseWriter, r *http.Request) {
	r.ParseForm()
	bytes, _ := ioutil.ReadAll(r.Body)

	team := &models.Team{}
	json.Unmarshal(bytes, &team)

	us1 := team.Member1
	us2 := team.Member2
	us3 := team.Member3
	chk1, chk2, chk3 := _CheckUniqeMember(us1, us2, us3, team.MatchId)

	mapData := make(map[string]interface{})
	if !(chk1 || chk2 || chk3) {
		team.Date = time.Now()
		team.Level = "TEAM"
		team.MemberUpdate = us1
		team.TimeUpdate = time.Now()
		team.Member2Status = false
		team.Member2Status = false

		err := models.Team_Save(team)

		match, _ := models.Match_ListByMatchId(team.MatchId)
		match.Team = append(match.Team, *team)
		models.Match_SettingsUpdate(&match)

		if err != nil {
			mapData["status"] = false
			mapData["message"] = err.Error()
		} else {
			mapData["status"] = true
			mapData["message"] = "สร้างทีมสำเร็จ"
		}
	} else {
		mapData["status"] = false
		if chk1 {
			mapData["message"] = "สร้างทีมไม่สำเร็จ<br />เนื่องจาก : หัวหน้าทีมสมัครซ้ำ"
		} else if chk2 {
			mapData["message"] = "สร้างทีมไม่สำเร็จ<br />เนื่องจาก : " + us2 + " สมัครซ้ำ"
		} else if chk3 {
			mapData["message"] = "สร้างทีมไม่สำเร็จ<br />เนื่องจาก : " + us3 + " สมัครซ้ำ"
		}
	}

	render.RenderJSON(w, mapData)
}

func Team_CheckRegistration(w http.ResponseWriter, r *http.Request) {
	us := r.URL.Query().Get("user")
	matchid, _ := strconv.Atoi(r.URL.Query().Get("matchid"))

	member, chk_head := models.Team_ListByMember(bson.M{"member1": us, "matchid": matchid})
	member1, chk_member1 := models.Team_ListByMember(bson.M{"member2": us, "matchid": matchid})
	member2, chk_member2 := models.Team_ListByMember(bson.M{"member3": us, "matchid": matchid})

	mapData := make(map[string]interface{})
	if chk_head || chk_member1 || chk_member2 {
		mapData["status"] = true
		mapData["head"] = chk_head

		members := make([]models.Member, 3)

		if chk_head {
			mapData["data"] = member
			members[0] = models.Member_ListByUser(member.Member1)
			members[1] = models.Member_ListByUser(member.Member2)
			members[2] = models.Member_ListByUser(member.Member3)
		} else if chk_member1 {
			mapData["data"] = member1
			members[0] = models.Member_ListByUser(member1.Member1)
			members[1] = models.Member_ListByUser(member1.Member2)
			members[2] = models.Member_ListByUser(member1.Member3)
		} else {
			mapData["data"] = member2
			members[0] = models.Member_ListByUser(member2.Member1)
			members[1] = models.Member_ListByUser(member2.Member2)
			members[2] = models.Member_ListByUser(member2.Member3)
		}

		mapData["members"] = members
	} else {
		mapData["status"] = false
	}

	render.RenderJSON(w, mapData)
}

func Team_AcceptMember(w http.ResponseWriter, r *http.Request) {
	q := r.URL.Query()
	member := string(q.Get("member"))
	user := string(q.Get("user"))
	teamid := string(q.Get("teamid"))

	team := models.Team_List(teamid)
	if member == "one" {
		team.Member2Status = true
	} else {
		team.Member3Status = true
	}
	team.TimeUpdate = time.Now()
	team.MemberUpdate = user

	models.Team_Update(&team)

	mapData := map[string]interface{}{
		"status":  true,
		"message": "ยืนยันการเข้าร่วมทีมเรียบร้อย",
	}
	render.RenderJSON(w, mapData)
}

func Team_ListAll(w http.ResponseWriter, r *http.Request) {
	t, _ := models.Team_ListAll()

	if t == nil {
		t = make([]interface{}, 0)
	}

	render.RenderJSON(w, t)
}

func Team_MatchListAll(w http.ResponseWriter, r *http.Request) {
	_, match := models.Match_ListAll()
	_, team := models.Team_ListAll()

	resultMatch := make([]map[string]interface{}, len(match))
	for i, v := range match {
		resultMatch[i] = map[string]interface{}{
			"id":   v.MatchId,
			"name": v.Name,
		}
	}

	matchid, err := strconv.Atoi(r.URL.Query().Get("matchid"))
	if err == nil {
		team = models.Team_ListByMatchId(matchid)
	}
	resultTeam := make([]map[string]string, len(team))
	for i, v := range team {
		resultTeam[i] = map[string]string{
			"id":   v.Id.Hex(),
			"name": v.TeamName,
		}
	}

	result := map[string]interface{}{
		"team":  resultTeam,
		"match": resultMatch,
	}

	render.RenderJSON(w, result)
}

func Team_CheckLogin(w http.ResponseWriter, r *http.Request) {
	q := r.URL.Query()
	id := string(q.Get("id"))
	pass := string(q.Get("pw"))

	m := models.Team_CheckLogin(id, pass)

	render.RenderJSON(w, m)
}

func Team_ListByMatchId(w http.ResponseWriter, r *http.Request) {
	matchid, _ := strconv.Atoi(r.URL.Query().Get("matchid"))

	team := models.Team_ListByMatchId(matchid)

	render.RenderJSON(w, team)
}

// route : "/team/generatepassword"
func Team_GeneratePassWord(w http.ResponseWriter, r *http.Request) {
	body, _ := ioutil.ReadAll(r.Body)

	var newPw []models.Data
	json.Unmarshal(body, &newPw)

	models.Team_GeneratePassWord(newPw)
}

func _CheckUniqeMember(us1, us2, us3 string, matchid int) (chk1, chk2, chk3 bool) {
	// _, teams := models.Team_ListAll()
	teams := models.Team_ListByMatchId(matchid)

	list := make([]string, len(teams)*2)
	for i, t := range teams {
		if i == 0 {
			list[i] = t.Member2
			list[i+1] = t.Member3
		} else {
			list = append(list, t.Member2, t.Member3)
		}
	}

	sort.Strings(list)
	i := sort.SearchStrings(list, us1)
	chk1 = (i < len(list) && list[i] == us1)

	j := sort.SearchStrings(list, us2)
	chk2 = (j < len(list) && list[j] == us2)

	k := sort.SearchStrings(list, us3)
	chk3 = (k < len(list) && list[k] == us3)

	return
}
