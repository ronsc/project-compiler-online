package controllers

import (
	// "fmt"
	"net/http"

	"math"
	"strconv"
	"strings"
	"time"

	"gopkg.in/mgo.v2/bson"

	"go-code/project-compiler-online/src/models"
	"go-code/project-compiler-online/src/render"
	// "../models"
	// "../render"
	// "cplonline/src/models"
	// "cplonline/src/render"
)

func ScoreBoard_Save(w http.ResponseWriter, r *http.Request) {
	data := &models.ScoreBoard{}
	// data.TeamId = "55953dfe69b6f61c08dc0859"
	data.TeamId = "55953e2d69b6f61c08dc085a"
	data.Solved = 1
	data.TotalTime = 40
	data.TotalSolved = 3

	models.ScoreBoard_Save(data)

	m := models.ScoreBoard_ListAll()
	render.RenderJSON(w, m)
}

func ScoreBoard_ListAll(w http.ResponseWriter, r *http.Request) {
	matchId, _ := strconv.Atoi(r.URL.Query().Get("matchid"))
	m, _ := models.Match_ListByMatchId(matchId)
	team := m.Team
	// _, team := models.Team_ListAll()

	result := make([]map[string]map[string]interface{}, len(team))
	for k, v := range team {
		t, _ := models.Team_ListByMember(bson.M{"member1": v.Member1, "member2": v.Member2, "member3": v.Member3, "matchid": v.MatchId})
		solved, totalsolved := models.TaskSubmit_CountSolvedByTeam(t.Id.Hex(), v.MatchId)
		totaltime := _SumTotalTime(t.Id.Hex())

		result[k] = map[string]map[string]interface{}{
			"detail": map[string]interface{}{
				"teamid":   v.Id.Hex(),
				"teamname": v.TeamName,
			},
			"submit": map[string]interface{}{
				"solved":      solved,
				"totalsovled": totalsolved,
				"totaltime":   totaltime,
				"tasks":       _SumTimeAllByCode(t.Id.Hex(), matchId),
				"first":       _FindFirstSubmitByTeamId(t.Id.Hex(), matchId),
			},
		}
	}

	render.RenderJSON(w, result)
}

func ScoreBoard_ListByTeam(w http.ResponseWriter, r *http.Request) {
	teamId := r.URL.Query().Get("teamid")
	matchId, _ := strconv.Atoi(r.URL.Query().Get("matchid"))

	team := models.Team_List(teamId)
	result := make(map[string]map[string]interface{})

	solved, totalsolved := models.TaskSubmit_CountSolvedByTeam(team.Id.Hex(), matchId)
	totaltime := _SumTotalTime(team.Id.Hex())

	result = map[string]map[string]interface{}{
		"detail": map[string]interface{}{
			"teamid":   team.Id.Hex(),
			"teamname": team.TeamName,
		},
		"submit": map[string]interface{}{
			"solved":      solved,
			"totalsovled": totalsolved,
			"totaltime":   totaltime,
			"tasks":       _SumTimeAllByCode(team.Id.Hex(), matchId),
		},
	}

	render.RenderJSON(w, result)
}

func ScoreBoard_ListByPass(w http.ResponseWriter, r *http.Request) {
	taskPass := models.TaskSubmit_FindByModeAndPass("MATCH")

	render.RenderJSON(w, taskPass)
}

func _SumTotalTime(teamid string) int {
	totaltime := float64(0.0)
	timeStart, failSubmit := _GetTimeStart()

	tasksubmit := models.TaskSubmit_MathchFindByTeamId(teamid)
	for _, v := range tasksubmit {
		if v.TeamId != "-" {
			// fmt.Println(v.TeamId, v.Code, math.Floor(v.Date.Sub(timeStart).Minutes()), v.Status)
			if v.Status == "pass" {
				taskDate := _ConvertTaskDateToDateNow(v.Date)
				totaltime += math.Floor(taskDate.Sub(timeStart).Minutes())
			} else {
				totaltime += float64(failSubmit)
			}
		}
	}
	// fmt.Println(totaltime)
	return int(totaltime)
}

func _SumTimeAllByCode(teamid string, matchid int) interface{} {
	m, _ := models.Match_ListByMatchId(matchid)
	task := m.Task
	// _, task := models.Task_FindAllByMode("MATCH")
	result := make(map[string]interface{})
	timeStart, _ := _GetTimeStart()

	for _, v := range task {
		tasksubmit := models.TaskSubmit_MatchFindByTeamIdAndCode(teamid, v.Code, matchid)

		list := make([]int, len(tasksubmit))
		for i, v := range tasksubmit {
			// fmt.Println(i, x.TeamId, x.Code, math.Floor(x.Date.Add(time.Hour * 24).Sub(timeStart).Minutes()), x.Status)
			if v.Status == "pass" {
				taskDate := _ConvertTaskDateToDateNow(v.Date)
				// fmt.Println(taskDate)
				list[i] = int(math.Floor(taskDate.Sub(timeStart).Minutes()))
			} else {
				list[i] = 0
			}
		}
		result[v.Code] = list
	}

	return result
}

func _SumTimePassByCode(teamid string, code string, matchid int) int {
	timePass := 0
	timeStart, _ := _GetTimeStart()

	tasksubmit := models.TaskSubmit_MatchFindByTeamIdAndCode(teamid, code, matchid)
	for _, v := range tasksubmit {
		taskDate := _ConvertTaskDateToDateNow(v.Date)
		timePass = int(math.Floor(taskDate.Sub(timeStart).Minutes()))
	}

	return timePass
}

func _FindFirstSubmitByTeamId(teamid string, matchid int) map[string]bool {
	taskPass := models.TaskSubmit_FindByModeAndPass("MATCH")
	result := make(map[string]bool)

	for _, v := range taskPass {
		if v.TeamId == teamid {
			min := _SumTimePassByCode(teamid, v.Code, matchid)
			status := true
			sendPass := models.TaskSubmit_GetSendAndPass("MATCH", v.Code)
			for _, vv := range sendPass {
				if _SumTimePassByCode(vv.TeamId, v.Code, matchid) < min {
					status = false
				}
			}

			if status {
				result[v.Code] = true
			} else {
				result[v.Code] = false
			}
		}
	}

	return result
}

func _GetTimeStart() (time.Time, int) {
	_, match := models.Match_List()
	clock := strings.Split(match.TimeStart, ":")
	year, month, day := time.Now().Date()
	hours, _ := strconv.Atoi(clock[0])
	min, _ := strconv.Atoi(clock[1])
	sec, _ := strconv.Atoi(clock[2])
	timeStart := time.Date(year, month, day, hours, min, sec, 0, time.Local)

	return timeStart, match.FailSubmit
}

func _ConvertTaskDateToDateNow(date time.Time) time.Time {
	year, month, day := time.Now().Date()
	hours, min, sec := date.Clock()

	// for debug
	timeStart, _ := _GetTimeStart()
	hours = timeStart.Hour()

	return time.Date(year, month, day, hours, min, sec, 0, time.Local)
}
