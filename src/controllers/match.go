package controllers

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"math"
	"net/http"
	"strconv"
	"strings"
	"time"

	"go-code/project-compiler-online/src/models"
	"go-code/project-compiler-online/src/render"
	// "../models"
	// "../render"
	// "cplonline/src/models"
	// "cplonline/src/render"
)

func Match_ListByMatchId(w http.ResponseWriter, r *http.Request) {
	matchid, _ := strconv.Atoi(r.URL.Query().Get("matchid"))

	_, result := models.Match_ListByMatchId(matchid)

	render.RenderJSON(w, result)
}

func Match_ListAll(w http.ResponseWriter, r *http.Request) {
	result, _ := models.Match_ListAll()

	render.RenderJSON(w, result)
}

func Match_GetTime(w http.ResponseWriter, r *http.Request) {
	mapData := make(map[string]interface{})
	matchid, _ := strconv.Atoi(r.URL.Query().Get("matchid"))
	result, _ := models.Match_ListByMatchId(matchid)

	y, m, d := result.DateMatch.Date()
	year, month, day := time.Now().Date()

	if y == year && m == month && d == day {
		clock := strings.Split(result.TimeStart, ":")
		hours, _ := strconv.Atoi(clock[0])
		min, _ := strconv.Atoi(clock[1])
		sec, _ := strconv.Atoi(clock[2])

		timeStart := time.Date(year, month, day, hours, min, sec, 0, time.Local)
		timestartDiff := timeStart.Sub(time.Now())
		timestartSec := math.Floor((timestartDiff.Hours() / (60 * 60)) + (timestartDiff.Minutes() / 60) + timestartDiff.Seconds())

		timeEnd := timeStart.Add(time.Duration(result.TimeHours) * time.Hour)
		timeendDiff := timeEnd.Sub(time.Now())
		timeendSec := math.Floor(timeendDiff.Hours()/(60*60)) + math.Floor(timeendDiff.Minutes()/60) + math.Floor(timeendDiff.Seconds())

		mapData["startseconds"] = strconv.FormatFloat(timestartSec, 'f', -1, 64)
		mapData["endseconds"] = strconv.FormatFloat(timeendSec, 'f', -1, 64)
		if timestartSec <= 0 {
			result.Status = "RUN"
			models.Match_SettingsUpdate(&result)
		}
		if timeendSec <= 0 {
			result.Status = "END"
			models.Match_SettingsUpdate(&result)
		}
		mapData["status"] = result.Status
		mapData["failsubmit"] = result.FailSubmit
	} else {
		mapData["status"] = "END"
	}

	render.RenderJSON(w, mapData)
}

func Match_Setup(w http.ResponseWriter, r *http.Request) {
	r.ParseForm()

	bytes, _ := ioutil.ReadAll(r.Body)
	data := &models.Match{}
	json.Unmarshal(bytes, &data)

	// set new value match
	matchid, _ := strconv.Atoi(r.URL.Query().Get("matchid"))
	match, _ := models.Match_ListByMatchId(matchid)
	match.TimeStart = data.TimeStart
	match.TimeEnd = data.TimeEnd
	match.TimeHours = data.TimeHours
	match.FailSubmit = data.FailSubmit
	match.Status = data.Status
	match.Details = data.Details

	match.Name = data.Name
	match.RegStart = data.RegStart
	match.RegEnd = data.RegEnd
	match.DateMatch = data.DateMatch
	match.Public = data.Public
	// update to db
	ret, _ := models.Match_SettingsUpdate(&match)

	render.RenderJSON(w, ret)
}

func Match_Save(w http.ResponseWriter, r *http.Request) {
	r.ParseForm()
	bytes, _ := ioutil.ReadAll(r.Body)

	data := &models.Match{}
	json.Unmarshal(bytes, &data)

	data.MatchId = (models.Match_Last().MatchId + 1)
	data.TimeStart = "00:00:00"
	data.TimeEnd = "00:00:00"
	data.TimeHours = 0
	data.FailSubmit = 0
	data.Status = "IDLE"
	data.Public = false
	data.Created = time.Now()

	models.Match_Save(data)
}

func Match_Delete(w http.ResponseWriter, r *http.Request) {
	matchid, _ := strconv.Atoi(r.URL.Query().Get("matchid"))

	result := make(map[string]interface{})
	result["status"] = true
	result["matchid"] = matchid

	match, _ := models.Match_ListByMatchId(matchid)
	team := len(match.Team)

	y, m, d := time.Now().Date()
	yy, mm, dd := match.DateMatch.Date()

	if y == yy && m == mm && d == dd {
		// check datematch
		result["status"] = false
		result["message"] = "วันนี้เป็นวันแข่งขันจริง"

		log.Println(y, m, d, " == ", yy, mm, dd)
	} else if team != 0 {
		// check team not null
		result["status"] = false
		result["message"] = "มีทีมสมัครเข้าแข่งขันแล้ว"
	} else {
		err := models.Match_Delete(matchid)
		if err != nil {
			result["status"] = false
			result["message"] = err.Error()

			log.Println(err.Error())
		}
	}

	render.RenderJSON(w, result)
}

func Match_Public(w http.ResponseWriter, r *http.Request) {
	match := models.Match_Public()

	render.RenderJSON(w, match)
}

func Match_ListByTaskCode(w http.ResponseWriter, r *http.Request) {
	code := r.URL.Query().Get("Code")
	matchid, _ := strconv.Atoi(r.URL.Query().Get("matchid"))

	task := &models.Task{}
	match, _ := models.Match_ListByMatchId(matchid)
	for _, v := range match.Task {
		if v.Code == code {
			task = &v
		}
	}

	render.RenderJSON(w, *task)
}

func _InitMatch() {
	m := &models.Match{}

	m.MatchId = 5
	m.Name = "init Match"
	m.RegStart = time.Now()
	m.RegEnd = time.Now()
	m.DateMatch = time.Now()
	m.TimeStart = "00:00:00"
	m.TimeEnd = "00:00:00"
	m.TimeHours = 0
	m.FailSubmit = 20
	m.Details = "not schedule..."
	m.Status = "IDLE"
	m.Public = false
	m.Created = time.Now()

	models.Match_Save(m)
}
