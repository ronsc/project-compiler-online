package models

import (
	// "fmt"
	"log"
	"time"

	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

type Team struct {
	Id            bson.ObjectId `json:"_id" bson:"_id,omitempty"`
	TeamName      string        `json:"teamname"`
	Member1       string        `json:"member1"`
	Member2       string        `json:"member2"`
	Member3       string        `json:"member3"`
	PassWord      string        `json:"password"`
	Level         string        `json:"level"`
	Date          time.Time     `json:"date"`
	MemberUpdate  string        `json:"memberupdate"`
	TimeUpdate    time.Time     `json:"timeupdate"`
	MatchId       int           `json:"matchid"`
	Member2Status bool          `json:"member2status"`
	Member3Status bool          `json:"member3status"`
}

type Data struct {
	TeamName string
	PassWord string
}

func Team_Save(t *Team) (err error) {
	session, _ := mgo.Dial(DB_URL)
	defer session.Close()

	c := session.DB(DB_NAME).C(C_TEAM)
	err = c.Insert(t)
	return
}

func Team_Update(t *Team) (err error) {
	session, _ := mgo.Dial(DB_URL)
	defer session.Close()
	c := session.DB(DB_NAME).C(C_TEAM)

	condition := bson.M{"_id": bson.ObjectIdHex(t.Id.Hex())}
	setChange := bson.M{"$set": t}
	err = c.Update(condition, setChange)
	return
}

func Team_ListAll() (result []interface{}, result2 []Team) {
	session, _ := mgo.Dial(DB_URL)
	defer session.Close()

	c := session.DB(DB_NAME).C(C_TEAM)

	c.Find(bson.M{}).Sort("date").All(&result)
	c.Find(bson.M{}).Sort("date").All(&result2)
	return
}

func Team_List(teamid string) (result Team) {
	session, _ := mgo.Dial(DB_URL)
	defer session.Close()

	c := session.DB(DB_NAME).C(C_TEAM)

	c.Find(bson.M{"_id": bson.ObjectIdHex(teamid)}).One(&result)
	return
}

func Team_ListByMember(query bson.M) (result Team, chk bool) {
	session, _ := mgo.Dial(DB_URL)
	defer session.Close()

	c := session.DB(DB_NAME).C(C_TEAM)

	c.Find(query).One(&result)

	count, _ := c.Find(query).Count()
	chk = true
	if count == 0 {
		chk = false
	}

	return
}

func Team_ListByMatchId(matchid int) (result []Team) {
	session, _ := mgo.Dial(DB_URL)
	defer session.Close()

	c := session.DB(DB_NAME).C(C_TEAM)

	c.Find(bson.M{"matchid": matchid}).All(&result)
	return
}

func Team_CheckLogin(id string, pw string) (result interface{}) {
	session, _ := mgo.Dial(DB_URL)
	defer session.Close()

	c := session.DB(DB_NAME).C(C_TEAM)

	c.Find(bson.M{"_id": bson.ObjectIdHex(id), "password": pw}).One(&result)
	return
}

func Team_GeneratePassWord(newPw []Data) {
	session, _ := mgo.Dial(DB_URL)
	defer session.Close()

	c := session.DB(DB_NAME).C(C_TEAM)

	for _, t := range newPw {
		condi := bson.M{"teamname": t.TeamName}
		change := bson.M{"$set": bson.M{"password": t.PassWord}}
		err := c.Update(condi, change)
		if err != nil {
			log.Println(err)
		}
	}
}
