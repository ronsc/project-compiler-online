package models

import (
	// "fmt"

	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

type ScoreBoard struct {
	Id          bson.ObjectId `json:"_id" bson:"_id,omitempty"`
	TeamId      string        `json:"teamid" bson:"teamid"`
	Solved      int           `json:"solved" bson:"solved"`
	TotalTime   int           `json:"totaltime" bson:"totaltime"`
	TotalSolved int           `json:"totalsolved" bson:"totalsolved"`
}

func ScoreBoard_Save(s *ScoreBoard) (err error) {
	session, _ := mgo.Dial(DB_URL)
	defer session.Close()

	c := session.DB(DB_NAME).C(C_SCORE_BOARD)
	err = c.Insert(s)
	return
}

func ScoreBoard_ListAll() (result []interface{}) {
	session, _ := mgo.Dial(DB_URL)
	defer session.Close()

	c := session.DB(DB_NAME).C(C_SCORE_BOARD)

	c.Find(bson.M{}).All(&result)
	return
}
