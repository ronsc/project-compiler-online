package models

import (
	// "encoding/json"
	// "fmt"
	"log"
	// "time"
	// "strconv"

	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

type Score struct {
	Id            bson.ObjectId `json:"_id" bson:"_id,omitempty"`
	Easy          int
	Medium        int
	Hard          int
	Min           int
	EasyMinus     float64
	MediumMinus   float64
	HardMinus     float64
	Times         int
	ScorePerLevel int
}

func Score_Save(s *Score) {
	session, _ := mgo.Dial(DB_URL)
	defer session.Close()

	c := session.DB(DB_NAME).C(C_SCORE)

	c.Insert(s)
}

func Score_List() (result interface{}, result2 Score) {
	session, _ := mgo.Dial(DB_URL)
	defer session.Close()

	c := session.DB(DB_NAME).C(C_SCORE)

	c.Find(bson.M{}).One(&result)
	c.Find(bson.M{}).One(&result2)
	return
}

func Score_Update(s *Score) (ret interface{}) {
	session, _ := mgo.Dial(DB_URL)
	defer session.Close()

	c := session.DB(DB_NAME).C(C_SCORE)

	condition := bson.M{"_id": bson.ObjectIdHex(s.Id.Hex())}
	setChange := bson.M{"$set": s}
	err := c.Update(condition, setChange)
	if err != nil {
		log.Println(err)
	}

	c.Find(bson.M{"_id": bson.ObjectIdHex(s.Id.Hex())}).One(&ret)
	return
}
