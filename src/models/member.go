package models

import (
	_ "encoding/json"
	// "fmt"
	"log"
	"time"

	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

type Member struct {
	Id       bson.ObjectId `json:"_id" bson:"_id,omitempty"`
	UserName string        `json:"username"`
	Email    string        `json:"email"`
	PassWord string        `json:"password"`
	Name     string        `json:"name"`
	LastName string        `json:"lastname"`
	Sex      string        `json:"sex"`
	CodeName string        `json:"codename"`
	BirthDay time.Time     `json:"birthday"`
	WebSite  string        `json:"website"`
	Province string        `json:"province"`
	Status   string        `json:"status"`
	FaceBook string        `json:"facebook"`
	Address  string        `json:"address"`
	Tel      string        `json:"tel"`
	Date     time.Time     `json:"date"`
	Level    string        `json:"level"`
}

func Member_Save(m *Member) {
	session, _ := mgo.Dial(DB_URL)
	defer session.Close()

	c := session.DB(DB_NAME).C(C_MEMBER)
	c.Insert(m)
}

func Member_ListAll() (result []interface{}, result2 []Member) {
	session, _ := mgo.Dial(DB_URL)
	defer session.Close()

	c := session.DB(DB_NAME).C(C_MEMBER)

	c.Find(bson.M{}).All(&result)
	c.Find(bson.M{"level": "USER"}).All(&result2)
	return
}

func Member_ListByUser(us string) (result Member) {
	session, _ := mgo.Dial(DB_URL)
	defer session.Close()

	c := session.DB(DB_NAME).C(C_MEMBER)

	c.Find(bson.M{"username": us}).One(&result)
	return
}

func Member_CheckLogin(us string, pw string) (result interface{}) {
	session, _ := mgo.Dial(DB_URL)
	defer session.Close()

	c := session.DB(DB_NAME).C(C_MEMBER)

	c.Find(bson.M{"username": us, "password": pw}).One(&result)
	return
}

func Member_CheckUser(us string) (result interface{}) {
	session, _ := mgo.Dial(DB_URL)
	defer session.Close()

	c := session.DB(DB_NAME).C(C_MEMBER)

	c.Find(bson.M{"username": us}).One(&result)
	return
}

func Member_Update(m *Member) (ret interface{}, err error) {
	session, _ := mgo.Dial(DB_URL)
	defer session.Close()

	c := session.DB(DB_NAME).C(C_MEMBER)

	condition := bson.M{"_id": bson.ObjectIdHex(m.Id.Hex())}
	setChange := bson.M{"$set": m}
	err = c.Update(condition, setChange)
	if err != nil {
		log.Println(err)
	}

	c.Find(bson.M{"_id": bson.ObjectIdHex(m.Id.Hex())}).One(&ret)
	return
}

func Member_Delete(us string) (err error) {
	session, _ := mgo.Dial(DB_URL)
	defer session.Close()

	c_member := session.DB(DB_NAME).C(C_MEMBER)
	c_ranking := session.DB(DB_NAME).C(C_RANKING)
	c_task_submit := session.DB(DB_NAME).C(C_TASK_SUBMIT)

	err = c_member.Remove(bson.M{"username": us})
	err = c_ranking.Remove(bson.M{"username": us})
	err = c_task_submit.Remove(bson.M{"username": us})

	return
}
