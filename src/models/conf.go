package models

const (
	DB_URL  string = "127.0.0.1:27017"
	DB_NAME string = "compileronline_test"

	C_MATCH        string = "match"
	C_MEMBER       string = "member"
	C_NOTIFICATION string = "notification"
	C_RANKING      string = "ranking"
	C_SCORE        string = "score"
	C_SCORE_BOARD  string = "scoreboard"
	C_TASK         string = "task"
	C_TASK_SUBMIT  string = "tasksubmit"
	C_TEAM         string = "team"
)
