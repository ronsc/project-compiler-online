package models

import (
	// "encoding/json"
	// "fmt"
	// "log"
	"time"
	// "strconv"

	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

type Notification struct {
	Id      bson.ObjectId `json:"_id" bson:"_id,omitempty"`
	Title   string
	Message string
	Date    time.Time
}

func Notification_Save(n *Notification) (err error) {
	session, _ := mgo.Dial(DB_URL)
	defer session.Close()

	c := session.DB(DB_NAME).C(C_NOTIFICATION)
	err = c.Insert(n)
	return
}

func Notification_ListAll() (result []Notification, count int) {
	session, _ := mgo.Dial(DB_URL)
	defer session.Close()

	c := session.DB(DB_NAME).C(C_NOTIFICATION)

	c.Find(bson.M{}).Sort("-date").All(&result)
	count, _ = c.Find(bson.M{}).Count()
	return
}
