package models

import (
	_ "encoding/json"
	// "fmt"
	"log"
	// "time"

	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

type Ranking struct {
	Id       bson.ObjectId `json:"_id" bson:"_id,omitempty"`
	UserName string
	CodeName string
	Point    float64
	Level    int
	Percen   float64
}

func Ranking_Save(r *Ranking) {
	session, _ := mgo.Dial(DB_URL)
	defer session.Close()

	c := session.DB(DB_NAME).C(C_RANKING)
	err := c.Insert(r)
	if err != nil {
		log.Println(err.Error())
	}
}

func Ranking_ListAll() (result []Ranking) {
	session, _ := mgo.Dial(DB_URL)
	defer session.Close()

	c := session.DB(DB_NAME).C(C_RANKING)

	c.Find(bson.M{}).All(&result)
	return
}

func Ranking_ListByUserName(username string) (result Ranking) {
	session, _ := mgo.Dial(DB_URL)
	defer session.Close()

	c := session.DB(DB_NAME).C(C_RANKING)

	c.Find(bson.M{"username": username}).One(&result)
	return
}

func Ranking_Update(r *Ranking) {
	session, _ := mgo.Dial(DB_URL)
	defer session.Close()

	c := session.DB(DB_NAME).C(C_RANKING)

	condition := bson.M{"_id": bson.ObjectIdHex(r.Id.Hex())}
	setChange := bson.M{"$set": r}
	err := c.Update(condition, setChange)
	if err != nil {
		log.Println(err)
	}
}
