package models

import (
	_ "encoding/json"
	// "fmt"
	// "log"
	"time"

	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

type Task struct {
	Id        bson.ObjectId `json:"_id" bson:"_id,omitempty"`
	Code      string        `json:"code"`
	Title     string        `json:"title"`
	Content   string        `json:"content"`
	Level     string        `json:"level"`
	Help      string        `json:"help"`
	Date      time.Time     `json:"date"`
	SendTotal int           `json:"sendtotal"`
	PassTotal int           `json:"passtotal"`
	Mode      string        `json:"mode"`
}

func Task_Save(p *Task) {
	session, _ := mgo.Dial(DB_URL)
	defer session.Close()
	c := session.DB(DB_NAME).C(C_TASK)

	c.Insert(p)
}

func Task_Update(t *Task) (err error) {
	session, _ := mgo.Dial(DB_URL)
	defer session.Close()
	c := session.DB(DB_NAME).C(C_TASK)

	condition := bson.M{"_id": bson.ObjectIdHex(t.Id.Hex())}
	setChange := bson.M{"$set": t}
	err = c.Update(condition, setChange)
	return
}

func Task_Delete(code string) error {
	session, _ := mgo.Dial(DB_URL)
	defer session.Close()
	c := session.DB(DB_NAME).C(C_TASK)

	err := c.Remove(bson.M{"code": code})

	if err != nil {
		return err
	}
	return nil
}

func Task_ListAll() (result []interface{}) {
	session, _ := mgo.Dial(DB_URL)
	defer session.Close()
	c := session.DB(DB_NAME).C(C_TASK)

	c.Find(bson.M{}).All(&result)
	return
}

func Task_CountLevel(level string) int {
	session, _ := mgo.Dial(DB_URL)
	defer session.Close()
	c := session.DB(DB_NAME).C(C_TASK)

	count, _ := c.Find(bson.M{"level": level}).Count()
	return count
}

func Task_FindByLevel(level string) (result []interface{}, result2 []Task) {
	session, _ := mgo.Dial(DB_URL)
	defer session.Close()
	c := session.DB(DB_NAME).C(C_TASK)

	c.Find(bson.M{"level": level}).All(&result)
	c.Find(bson.M{"level": level}).All(&result2)
	return
}

func Task_TrainFindByLevel(level string) (result []Task) {
	session, _ := mgo.Dial(DB_URL)
	defer session.Close()
	c := session.DB(DB_NAME).C(C_TASK)

	c.Find(bson.M{"level": level, "mode": "TRAIN"}).All(&result)
	return
}

func Task_FindByCode(code string) (result interface{}, result2 Task) {
	session, _ := mgo.Dial(DB_URL)
	defer session.Close()
	c := session.DB(DB_NAME).C(C_TASK)

	c.Find(bson.M{"code": code}).One(&result)
	c.Find(bson.M{"code": code}).One(&result2)
	return
}

func Task_FindAllByMode(mode string) (result []interface{}, result2 []Task) {
	session, _ := mgo.Dial(DB_URL)
	defer session.Close()
	c := session.DB(DB_NAME).C(C_TASK)

	c.Find(bson.M{"mode": mode}).All(&result)
	c.Find(bson.M{"mode": mode}).All(&result2)
	return
}
