package models

import (
	_ "encoding/json"
	// "fmt"
	// "log"
	"time"

	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

type TaskSubmit struct {
	Id       bson.ObjectId `json:"_id" bson:"_id,omitempty"`
	Code     string
	Date     time.Time
	UserName string
	Lang     string
	Result   string
	Status   string
	Mode     string
	TeamId   string
	MatchId  int
}

func TaskSubmit_Save(p *TaskSubmit) {
	session, _ := mgo.Dial(DB_URL)
	defer session.Close()

	c := session.DB(DB_NAME).C(C_TASK_SUBMIT)
	c.Insert(p)
}

func TaskSubmit_ListAll() (result []interface{}, result2 []TaskSubmit) {
	session, _ := mgo.Dial(DB_URL)
	defer session.Close()

	c := session.DB(DB_NAME).C(C_TASK_SUBMIT)

	c.Find(bson.M{}).Sort("-date").All(&result)
	c.Find(bson.M{}).Sort("-date").All(&result2)
	return
}

func TaskSubmit_FindByMode(mode string) (result []TaskSubmit) {
	session, _ := mgo.Dial(DB_URL)
	defer session.Close()

	c := session.DB(DB_NAME).C(C_TASK_SUBMIT)

	c.Find(bson.M{"mode": mode}).Sort("-date").All(&result)
	return
}

func TaskSubmit_FindTrainByCode(code string, user string) (result []TaskSubmit) {
	session, _ := mgo.Dial(DB_URL)
	defer session.Close()

	c := session.DB(DB_NAME).C(C_TASK_SUBMIT)

	if user == "" {
		c.Find(bson.M{"mode": "TRAIN", "code": code}).Sort("-date").All(&result)
	} else {
		c.Find(bson.M{"mode": "TRAIN", "code": code, "username": user}).Sort("-date").All(&result)
	}
	return
}

func TaskSubmit_FindTrainByUser(user string) (result []TaskSubmit) {
	session, _ := mgo.Dial(DB_URL)
	defer session.Close()

	c := session.DB(DB_NAME).C(C_TASK_SUBMIT)

	if user == "" {
		c.Find(bson.M{"mode": "TRAIN"}).Sort("-date").All(&result)
	} else {
		c.Find(bson.M{"mode": "TRAIN", "username": user}).Sort("-date").All(&result)
	}
	return
}

func TaskSubmit_FindByModeAndPass(mode string) (result []TaskSubmit) {
	session, _ := mgo.Dial(DB_URL)
	defer session.Close()

	c := session.DB(DB_NAME).C(C_TASK_SUBMIT)

	c.Find(bson.M{"mode": mode, "status": "pass"}).Sort("-date").All(&result)
	return
}

func TaskSubmit_MathchFindByCode(code string, matchid int) (result []TaskSubmit) {
	session, _ := mgo.Dial(DB_URL)
	defer session.Close()

	c := session.DB(DB_NAME).C(C_TASK_SUBMIT)

	c.Find(bson.M{"code": code, "mode": "MATCH", "matchid": matchid}).Sort("date").All(&result)
	return
}

func TaskSubmit_MathchFindByCodePass(code string, matchid int) (result []TaskSubmit) {
	session, _ := mgo.Dial(DB_URL)
	defer session.Close()

	c := session.DB(DB_NAME).C(C_TASK_SUBMIT)

	c.Find(bson.M{"code": code, "mode": "MATCH", "status": "pass", "matchid": matchid}).Sort("date").All(&result)
	return
}

func TaskSubmit_MathchFindByTeamId(teamid string) (result []TaskSubmit) {
	session, _ := mgo.Dial(DB_URL)
	defer session.Close()

	c := session.DB(DB_NAME).C(C_TASK_SUBMIT)

	c.Find(bson.M{"teamid": teamid, "mode": "MATCH"}).All(&result)
	return
}

func TaskSubmit_TrainFindByUserName(username string) (result []TaskSubmit, pass []TaskSubmit, notpass []TaskSubmit) {
	session, _ := mgo.Dial(DB_URL)
	defer session.Close()

	c := session.DB(DB_NAME).C(C_TASK_SUBMIT)

	c.Find(bson.M{"username": username, "mode": "TRAIN"}).All(&result)
	c.Find(bson.M{"username": username, "mode": "TRAIN", "status": "pass"}).All(&pass)
	c.Find(bson.M{"username": username, "mode": "TRAIN", "status": "notpass"}).All(&notpass)
	return
}

func TaskSubmit_TrainFindByUserNameAndCode(username string, code string) (result []TaskSubmit) {
	session, _ := mgo.Dial(DB_URL)
	defer session.Close()

	c := session.DB(DB_NAME).C(C_TASK_SUBMIT)

	c.Find(bson.M{"username": username, "code": code, "mode": "TRAIN"}).All(&result)
	return
}

func TaskSubmit_MatchFindByTeamIdAndCode(teamid, code string, matchid int) (result []TaskSubmit) {
	session, _ := mgo.Dial(DB_URL)
	defer session.Close()

	c := session.DB(DB_NAME).C(C_TASK_SUBMIT)

	c.Find(bson.M{"teamid": teamid, "code": code, "mode": "MATCH", "matchid": matchid}).All(&result)
	return
}

func TaskSubmit_CountCodeNotPassTrainMode(username string, code string) int {
	session, _ := mgo.Dial(DB_URL)
	defer session.Close()

	c := session.DB(DB_NAME).C(C_TASK_SUBMIT)

	count, _ := c.Find(bson.M{"username": username, "code": code, "mode": "TRAIN", "status": "notpass"}).Count()
	return count
}

func TaskSubmit_GetTimeSendAndPass(mode string, code string) (int, int) {
	session, _ := mgo.Dial(DB_URL)
	defer session.Close()

	c := session.DB(DB_NAME).C(C_TASK_SUBMIT)
	pass, _ := c.Find(bson.M{"mode": mode, "code": code, "status": "pass"}).Count()
	notp, _ := c.Find(bson.M{"mode": mode, "code": code, "status": "notpass"}).Count()

	return (pass + notp), (pass)
}

func TaskSubmit_GetSendAndPass(mode string, code string) (result []TaskSubmit) {
	session, _ := mgo.Dial(DB_URL)
	defer session.Close()

	c := session.DB(DB_NAME).C(C_TASK_SUBMIT)
	c.Find(bson.M{"mode": mode, "code": code, "status": "pass"}).All(&result)
	return
}

func TaskSubmit_CountSolvedByTeam(teamid string, matchid int) (solved int, totalsolved int) {
	session, _ := mgo.Dial(DB_URL)
	defer session.Close()

	c := session.DB(DB_NAME).C(C_TASK_SUBMIT)

	solved, _ = c.Find(bson.M{"teamid": teamid, "status": "pass", "mode": "MATCH", "matchid": matchid}).Count()
	totalsolved, _ = c.Find(bson.M{"teamid": teamid, "mode": "MATCH", "matchid": matchid}).Count()
	return
}
