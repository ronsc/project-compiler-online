package models

import (
	// "encoding/json"
	// "fmt"
	"log"
	// "time"
	// "strconv"
	"time"

	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

type Match struct {
	Id           bson.ObjectId `json:"_id" bson:"_id,omitempty"`
	MatchId      int
	Name         string
	RegStart     time.Time
	RegEnd       time.Time
	DateMatch    time.Time
	TimeStart    string
	TimeEnd      string
	TimeHours    int
	FailSubmit   int
	Details      string
	Status       string
	Public       bool
	Created      time.Time
	Team         []Team
	Task         []Task
	Notification []Notification
}

func Match_Save(m *Match) {
	session, _ := mgo.Dial(DB_URL)
	defer session.Close()

	c := session.DB(DB_NAME).C(C_MATCH)
	c.Insert(m)
}

func Match_List() (result interface{}, result2 Match) {
	session, _ := mgo.Dial(DB_URL)
	defer session.Close()

	c := session.DB(DB_NAME).C(C_MATCH)

	c.Find(bson.M{}).One(&result)
	c.Find(bson.M{}).One(&result2)
	return
}

func Match_ListAll() (result []interface{}, result2 []Match) {
	session, _ := mgo.Dial(DB_URL)
	defer session.Close()

	c := session.DB(DB_NAME).C(C_MATCH)

	c.Find(bson.M{}).All(&result)
	c.Find(bson.M{}).All(&result2)
	return
}

func Match_ListByMatchId(matchid int) (result Match, result2 interface{}) {
	session, _ := mgo.Dial(DB_URL)
	defer session.Close()

	c := session.DB(DB_NAME).C(C_MATCH)

	c.Find(bson.M{"matchid": matchid}).One(&result)
	c.Find(bson.M{"matchid": matchid}).One(&result2)
	return
}

func Match_Last() (result Match) {
	session, _ := mgo.Dial(DB_URL)
	defer session.Close()

	c := session.DB(DB_NAME).C(C_MATCH)

	c.Find(bson.M{}).Sort("-created").One(&result)
	return
}

func Match_SettingsUpdate(s *Match) (ret interface{}, err error) {
	session, _ := mgo.Dial(DB_URL)
	defer session.Close()

	c := session.DB(DB_NAME).C(C_MATCH)

	condition := bson.M{"matchid": s.MatchId}
	setChange := bson.M{"$set": s}
	err = c.Update(condition, setChange)
	if err != nil {
		log.Println(err)

		ret = nil
		return
	}

	err = c.Find(bson.M{"matchid": s.MatchId}).One(&ret)
	return
}

func Match_Public() (result []interface{}) {
	session, _ := mgo.Dial(DB_URL)
	defer session.Close()

	c := session.DB(DB_NAME).C(C_MATCH)

	c.Find(bson.M{"public": true}).Sort("datematch").All(&result)
	return
}

func Match_Delete(matchid int) (err error) {
	session, _ := mgo.Dial(DB_URL)
	defer session.Close()

	c := session.DB(DB_NAME).C(C_MATCH)

	err = c.Remove(bson.M{"matchid": matchid})
	return
}
