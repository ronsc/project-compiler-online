package render

import (
	"encoding/json"
	"net/http"

	"fmt"
	"io"
	"log"
	"os"
	"strings"
)

func RenderJSON(w http.ResponseWriter, data interface{}) {
	result, _ := json.Marshal(data)
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.Write(result)
}

func RenderTEXT(w http.ResponseWriter, data string) {
	w.Header().Set("Content-Type", "text/plain; charset=UTF-8")
	w.Write([]byte(data))
}

func RenderHTML(w http.ResponseWriter, data []byte) {
	w.Header().Set("Content-Type", "text/html; charset=UTF-8")
	_, err := w.Write(data)
	if err != nil {
		log.Println(err.Error())
	}
}

func RenderUPLOAD(w http.ResponseWriter, r *http.Request, mapData map[string]string) {
	var (
		formFile = mapData["formFile"]
		category = mapData["category"]
		name     = mapData["name"]
		dir      = mapData["dir"]
	)
	// the FormFile function takes in the POST input id file
	file, header, err := r.FormFile(formFile)
	if err != nil {
		fmt.Fprintln(w, err)
		return
	}
	defer file.Close()

	// upload by username
	usname := r.URL.RawQuery

	// path to upload file
	var osPath string
	if category == "task" {
		osPath = "uploads/task/" + dir + "/"
	} else {
		osPath = "www/uploads/" + usname + "/" + dir + "/"
	}

	// split type of image
	s := strings.Split(header.Filename, ".")
	fileType := s[len(s)-1]

	// create dir
	os.MkdirAll(osPath, 0644)

	// file name
	var fileName string
	if category == "task" {
		fileName = name
	} else {
		fileName = name + "." + fileType
	}

	// create file
	out, err := os.Create(osPath + fileName)
	if err != nil {
		fmt.Fprintf(w, "Unable to create the file for writing. Check your write access privilege")
		log.Println(err)
		return
	}
	defer out.Close()

	// write the content from POST to the file
	_, err = io.Copy(out, file)
	if err != nil {
		fmt.Fprintln(w, err)
		return
	}

	fmt.Fprintf(w, "File uploaded successfully : ")
	fmt.Fprintf(w, header.Filename)
	fmt.Fprintf(w, "\n")
}

func CopyFile(src, dst string) (err error) {
	sfi, err := os.Stat(src)
	if err != nil {
		return
	}
	if !sfi.Mode().IsRegular() {
		// cannot copy non-regular files (e.g., directories,
		// symlinks, devices, etc.)
		return fmt.Errorf("CopyFile: non-regular source file %s (%q)", sfi.Name(), sfi.Mode().String())
	}
	dfi, err := os.Stat(dst)
	if err != nil {
		if !os.IsNotExist(err) {
			return
		}
	} else {
		if !(dfi.Mode().IsRegular()) {
			return fmt.Errorf("CopyFile: non-regular destination file %s (%q)", dfi.Name(), dfi.Mode().String())
		}
		if os.SameFile(sfi, dfi) {
			return
		}
	}
	if err = os.Link(src, dst); err == nil {
		return
	}
	err = copyFileContents(src, dst)
	return
}

func copyFileContents(src, dst string) (err error) {
	in, err := os.Open(src)
	if err != nil {
		return
	}
	defer in.Close()
	out, err := os.Create(dst)
	if err != nil {
		return
	}
	defer func() {
		cerr := out.Close()
		if err == nil {
			err = cerr
		}
	}()
	if _, err = io.Copy(out, in); err != nil {
		return
	}
	err = out.Sync()
	return
}
