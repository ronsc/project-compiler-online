module.exports = function(grunt) {
	grunt.initConfig({
		watch: {
			options: {
				livereload: true
			},
			main: {
				files: ['*.html','views/*.html','views/*/*.html', 'views/layouts/*.html']
			},
			js: {
				files: ['assets/js/*.js', 'assets/js/*/*.js', 'scripts/*.js', 'scripts/*/*.js']
			}
		}
	})

	grunt.loadNpmTasks('grunt-contrib-watch')

	grunt.registerTask('default', ['watch'])
}