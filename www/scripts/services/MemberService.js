app.service('Member', function($http, Auth) {
	var userNameList = []

	return {
		initUserName: function(ignore_username) {
			userNameList = []
			$http.get('/member/listall').success(function(members) {
				angular.forEach(members.data, function(value, key) {
					if(value.username != ignore_username && value.username != Auth.isLogin().username && value.level == 'USER') {
						this.push(value.username)
					}
				}, userNameList)
			})
		},
		getUserName: function() {
			return userNameList
		}
	}
})