app.config(function($routeProvider, $locationProvider, NotificationProvider, FacebookProvider) {
	$routeProvider
	.when('/', {
		templateUrl : 'views/home.html',
		controller 	: 'HomeCtrl'
	})
	.when('/team/reg', {
		templateUrl : 'views/team/match_list.html',
		controller 	: 'TeamController'
	})
	.when('/team/reg/:MatchId', {
		templateUrl : 'views/team/match_reg.html',
		controller 	: 'TeamController'
	})
	.when('/team/match', {
		templateUrl : 'views/team/match_team.html',
		controller 	: 'TeamMatchController'
	})
	.when('/team/login', {
		templateUrl : 'views/team/match_login.html',
		controller 	: 'TeamMatchController'
	})
	.when('/match/scoreboard', {
		templateUrl : 'views/match/scoreboard.html',
		controller 	: 'ScoreBoardController'
	})
	.when('/member/reg', {
		templateUrl : 'views/member/reg_member.html',
		controller 	: 'MemberController'
	})
	.when('/member/profile', {
		templateUrl : 'views/member/profile.html',
		controller 	: 'MemberController'
	})
	.when('/profile/public/:UserName', {
		templateUrl : 'views/member/public_profile.html',
		controller 	: 'MemberController'
	})
	.when('/admin/manage/team/genpassword', {
		templateUrl : 'views/admin/genpassword.html',
		controller 	: 'AdminController'
	})
	.when('/admin/manage/score', {
		templateUrl : 'views/admin/manage_task_score.html',
		controller 	: 'ScoreController'
	})
	.when('/admin/manage/task', {
		templateUrl : 'views/admin/manage_task.html',
		controller 	: 'AdminController'
	})
	.when('/admin/manage/task/create/train', {
		templateUrl : 'views/admin/manage_task_create.html',
		controller 	: 'AdminController'
	})
	.when('/admin/manage/task/create/:Mode/:MatchId', {
		templateUrl : 'views/admin/manage_task_create.html',
		controller 	: 'AdminController'
	})
	.when('/admin/manage/match', {
		templateUrl : 'views/admin/manage_match.html',
		controller 	: 'ManageMatchController'
	})
	.when('/admin/manage/match/:MatchId', {
		templateUrl : 'views/admin/manage_match_view.html',
		controller 	: 'MatchController'
	})
	.when('/admin/manage/member', {
		templateUrl : 'views/admin/manage_member.html',
		controller 	: 'AdminController'
	})
	.when('/task', {
		templateUrl : 'views/task/main.html',
		controller 	: 'TaskController'
	})
	.when('/task/:Mode', {
		templateUrl : 'views/task/main.html',
		controller 	: 'TaskController',
		controllerAs: 'task'
	})
	.when('/task/view/:Code', {
		templateUrl : 'views/task/view.html',
		controller 	: 'TaskController'
	})
	.when('/task/view/submit/:Path', {
		templateUrl : 'views/task/view_submit.html',
		controller 	: 'TaskSubmitController'
	})
	.when('/task/submit/:Code', {
		templateUrl : 'views/task/submit.html',
		controller 	: 'TaskSubmitController'
	})
	.when('/404', {
		templateUrl : 'views/404.html'
	})
	.when('/logout', {})
	.otherwise({
		redirectTo : '/404'
	})

	$locationProvider.html5Mode({
	  	enabled: false,
	  	requireBase: false
	})

	NotificationProvider.setOptions({
        delay: 10000,
        startTop: 20,
        startRight: 20,
        verticalSpacing: 20,
        horizontalSpacing: 20,
        positionX: 'left',
        positionY: 'bottom'
    })

    FacebookProvider.init('1446967715624859')
})