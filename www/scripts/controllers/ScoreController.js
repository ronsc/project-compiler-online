app.controller('ScoreController', function($scope, $http, Notification) {
	
	$scope.init = function() {
		$http.get('/score/list').success(function(score) {
			$scope.score = score
		})
	}

	$scope.Score_Update = function() {
		$scope.score.easy 			= parseInt($scope.score.easy)
		$scope.score.medium 		= parseInt($scope.score.medium)
		$scope.score.hard 			= parseInt($scope.score.hard)
		$scope.score.min 			= parseInt($scope.score.min)
		$scope.score.easyminus 		= parseFloat($scope.score.easyminus)
		$scope.score.mediumminus 	= parseFloat($scope.score.mediumminus)
		$scope.score.hardminus 		= parseFloat($scope.score.hardminus)
		$scope.score.times 			= parseInt($scope.score.times)

		$http.post('/score/update', $scope.score).success(function(score) {
			$scope.score = score
			Notification.success({
				message: 'อัพเดตสำเร็จ', 
				replaceMessage: true,
				delay: 3000
			})
		})
	}

	// add class -sm for text-input, button
	$('input:text').addClass('input-sm')
	$('.btn').addClass('btn-sm')
	$('p').addClass('hidden-xs')
})