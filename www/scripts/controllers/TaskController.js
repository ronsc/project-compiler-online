app.controller('TaskController', 
	function($scope, $http, $routeParams, Auth, $location, $timeout, $filter, Facebook) {

	this.params = $routeParams
	$scope.params = $routeParams
	$scope.user = Auth.isLogin().username

	$scope.init = function() {
		$( "#accordion" ).accordion({
	      heightStyle: "content"
	    })

	    $scope.taskMode('EASY')
	}

	$scope.taskSubmitListAll = function() {
		$http.get('/tasksubmit/listall').success(function(datas) {
			$scope.tasksubmits = datas
		})
	}

	$scope.taskMode = function(level) {
		$http.get('/task/list/user?user='+$scope.user).success(function(tasks) {
			$scope.tasks = tasks
		})
	}

	$scope.showMenu = true
	$scope.taskView = function() {
		if($routeParams.mode != undefined) {
			$scope.showMenu = false

			$http.get('/match/task?' + $.param($routeParams)).success(function(task) {
				$scope.task = task
			})
		} else {
			$scope.Code = $routeParams.Code
			$http.get('/task/list/code?code=' + $scope.Code).success(function(task) {
				$scope.task = task
			})
		}

		$scope.absUrl = $location.absUrl()
		$scope.absUrl = 'http://golang-ronsc.rhcloud.com/' 
		// console.log($scope.absUrl)
	}

	// Facebook API 
	$scope.fbShare = function() {
		Facebook.ui({
		  	method: 'share',
		  	href: $scope.absUrl
		}, function(response){
			console.log(response)
		})
	}
})