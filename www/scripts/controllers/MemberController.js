app.controller('MemberController', function($scope, $http, $location, 
	Auth, $filter, $interval, $routeParams, Notification, $timeout) {
	$scope.user = Auth.isLogin()

	$scope.imgProfile = {
		'background-size': 'cover',
		'background-image':'url(uploads/'+$scope.user.username+'/profile/profile.jpg)',
		'height': '180px'
	}

	$scope.user.birthday = new Date($filter('date')($scope.user.birthday, 'yyyy-MM-dd'))

	$scope.reg_status = {
		"general": "บุคคลทั่วไป",
		"teacher": "ครู / อาจารย์",
		"student": "นักเรียน / นักศึกษา"
	}

	$(document).ready(function(){
		var $sort = $('div.row.well>div')
		$sort.sortable({
			connectWith: 'div.row.well>div>div.thumbnail',
			handle: '.portlet-header',
			placeholder: "portlet-placeholder ui-corner-all",
			cursor: "move"
		})

		$('input,select').addClass('input-sm')
	})
	
	$scope.initProfile = function() {
		$http.get('/tasksubmit/list/train/username?USERNAME='+$scope.user.username).success(function(datas){
			$scope.profileData = datas
			// console.log(datas)

			var persen = $scope.profileData.ranking.Percen
			$('#pgLevel').css('width', persen+'%').attr('aria-valuenow', persen)
		})
	}

	$scope.initPublicProfile = function() {
		var username = $routeParams.UserName

		$http.get('/member/finduser?us='+username).success(function(data){
			$scope.user = data
		})

		$http.get('/tasksubmit/list/train/username?USERNAME='+username).success(function(datas){
			$scope.profileData = datas

			var persen = $scope.profileData.ranking.Percen
			$('#pgLevel').css('width', persen+'%').attr('aria-valuenow', persen)
		})
	}

	// บันทึก ข้อมูลการสมัครสมาชิก
	$scope.save = function() {
		var pw 		= $scope.member.password,
			repw 	= $scope.member.repassword

		// ตรวจสอบว่า รหัสผ่านตรงกันหรือไม่ - กดปุ่ม submit
		if (pw != repw) {
			$scope.msgError = 'รหัสผ่านไม่ตรงกัน'
		} else {
			$scope.msgError = ''

			// ตรวจสอบว่า ผู้ใช้งานซ้ำหรือไม่
			$http.get('/member/checkuser?us=' + $scope.member.username).success(function(data) {
				if(data == 'true') {
					// บันทึกข้อมูลลง database
					$http.post('/member/registration', $scope.member).success(function(data) {
						Notification.success({
							message: 'ลงทะเบียน สำเร็จ', 
							replaceMessage: true,
							delay: 5000
						})

						$timeout(function() {
							if($scope.user.level == 'ADMIN') {
								$location.path('/admin/manage/member')
							} else {
								$location.path('/')
							}
						}, 2000);
					})
				} else {
					Notification.error({
						message: 'ลงทะเบียน ไม่สำเร็จ', 
						replaceMessage: true,
						delay: 5000
					})
				}
			})
		}
	}

	// ตรวจสอบว่า ผู้ใช้งานซ้ำหรือไม่ - กดปุ่ม ตรวจสอบ
	$scope.msgUser = {}
	$scope.member = {}
	$scope.member.sex = 'M'
	$scope.member.level = 'USER'
	$scope.check = function() {
		if($scope.member.username == undefined) {
			Notification.error({
				message: 'กรุณากรอก ชื่อผู้ใช้งาน',
				delay: 3000
			})
		} else {
			$http.get('/member/checkuser?us=' + $scope.member.username)
			.success(function(data) {
				if(data == 'false') {
					$scope.msgUser = {
						msg: 'ชื่อผู้ใช้งานต้องไม่ซ้ำ',
						c: 'danger'
					}
				} else {
					$scope.msgUser = {
						msg: 'สามารถใช้ ชื่อนี้ได้',
						c: 'success'
					}
				}
			})
		}
	}

	$scope.listall = function() {
		$http.get('/member/listall').success(function(members) {
			$scope.members = members
		})
	}

	// แก้ไขข้อมูลส่วนตัว
	$scope.Update = function() {
		console.log($scope.user)
		$http.post('/member/updatedata', $scope.user).success(function(user) {
			// console.log(user)
			Auth.setUser(user)
			$scope.update = false
		})
	}
	
	$scope.CancelUpdate = function() {
		window.location.reload()
		$scope.update = false
	}

	// อัพโหลดภาพ โปรไฟล์
	$scope.upload = function() {
		var file = document.getElementById('inputFile').files[0]
        // console.log('file is ' + JSON.stringify(file))
        var uploadUrl = "/member/uploadimage?" + $scope.user.username
        //fileUpload.uploadFileToUrl(file, uploadUrl)
        
        var fd = new FormData()
        // append file
  		fd.append("img", file)
    
        $http.post(uploadUrl, fd, {
            transformRequest: angular.identity,
            headers: {
            	'Content-Type': undefined
            }
        }).success(function(){ 
        	window.location.reload()
        })
	}

	// Change Password
	// Show Modal Form Change Password
	$scope.modalPassword = function() {
		$scope.old_pw = ''
		$scope.new_pw = ''
		$scope.re_pw = ''

		$('#myModal').modal('toggle')
	}
	// Update Password to db
	$scope.changePassword = function() {
		var old_pw = $scope.old_pw
		var new_pw = $scope.new_pw
		var re_pw = $scope.re_pw

		if(!angular.isDefined(new_pw) || !angular.isDefined(new_pw) || !angular.isDefined(re_pw)) {
			Notification.error({
				message: 'กรุณากรอก รหัสผ่าน',
				replaceMessage: true,
				delay: 3000
			})
			return false
		} else if(!angular.equals(new_pw, re_pw)) {
			Notification.error({
				message: 'รหัสผ่านใหม่ไม่ตรงกัน',
				replaceMessage: true,
				delay: 3000
			})
			return false
		} else {
			var data = {
				user: $scope.user.username,
				old_pw: old_pw,
				new_pw: new_pw
			}

			$http.get('/member/changepassword?' + $.param(data)).success(function(data) {
				if(data.status) {
					Notification.success({
						message: data.message,
						replaceMessage: true,
						delay: 3000
					})
					// toggle modal
					$scope.modalPassword()
				} else {
					Notification.error({
						message: data.message,
						replaceMessage: true,
						delay: 3000
					})
				}
			})
		}
	}
})