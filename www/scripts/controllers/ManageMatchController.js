app.controller('ManageMatchController', function($scope, $http, Notification) {
    $scope.initManageMatch = function() {
        $(document).ready( function() {
            $('#message').ckeditor()
        })

        $http.get('/task/list/mode?mode=MATCH').success(function(tasks) {
            $scope.tasks = tasks
        })

        $http.get('/match/listall').success(function(match) {
            $scope.matchs = match
        })
    }

    // Save Match
    $scope.MatchSave = function() {
        $scope.match.details = CKEDITOR.instances.message.getData()

        $http.post('/match/save', $scope.match).success(function(data) {
            Notification.success({
                message: 'สร้างแมทซ์ สำเร็จ', 
                replaceMessage: true,
                delay: 5000
            })
            // clear data
            $scope.match = {}
            CKEDITOR.instances.message.setData('')
            // hide modal
            $('#myModal').modal('hide')
            // reload data
            $http.get('/match/listall').success(function(match) {
                $scope.matchs = match
            })
        })
    }

    $scope.MatchDelete = function(matchid) {
        console.log(matchid)

        $("#dialog-confirm").dialog({
            resizable: false,
            modal: true,
            width: '350px',
            buttons: {
                "ยืนยัน": function() {
                    $(this).dialog("close")

                    $http.get('/match/delete?matchid=' + matchid).success(function(data) {
                        console.log(data)
                        if(data.status) {
                            // show message
                            Notification.success({
                                message: 'ลบแมทซ์ สำเร็จ', 
                                replaceMessage: true,
                                delay: 5000
                            })

                            // reload data
                            $http.get('/match/listall').success(function(match) {
                                $scope.matchs = match
                            })
                        } else {
                            Notification.error({
                                message: 'ลบแมทซ์ ไม่สำเร็จ<br />เนื่องจาก : ' + data.message, 
                                replaceMessage: true,
                                delay: 5000
                            })
                        }
                    })
                },
                "ยกเลิก": function() {
                    $(this).dialog("close")
                }
            }
        })
    }
})