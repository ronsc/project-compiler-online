app.controller('TeamMatchController', function($scope, $http, $location, $timeout, $interval, Notification, Auth) {
	
	$scope.chs = [
        'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 
        'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'
    ]
	$scope.results = [
		[
			{result: 'NP'},
			{result: 'NP'}, 
			{result: 'P'}
		],
		[
			{result: 'N'}
		],
		[
			{result: 'NP'},
			{result: 'NP'}, 
			{result: 'NP'}, 
			{result: 'NP'}, 
			{result: 'P'}
		],
	]

	$scope.matchid = Auth.isLoginTeam().matchid

	// init for team match
	$scope.taskTotal = 0
	$scope.taskPass = 0
	$scope.initMatch = function(){
		// call time to start match
		$scope.timeMath()
		$scope.TEAM = Auth.isLoginTeam()

		// realtime score
		$scope.scoreBoard()

		// $http.get('/task/list/mode?mode=MATCH').success(function(tasks) {
		// 	if(tasks != null){
		// 		$scope.tasks = tasks
		// 		$scope.taskTotal = tasks.length
		// 	}
		// })

		$http.get('/match/list?matchid=' + $scope.matchid).success(function(data) {
			if(data != null){
				$scope.tasks = data.task
				$scope.taskTotal = data.task.length
			}
		})

		$http.get('/notification/listall?matchid=' + $scope.matchid).success(function(datas){
			if(datas != null){
				$scope.notificationCount = datas.length
			}else{
				$scope.notificationCount = 0
			}
			
			$scope.notificationDatas = datas
		})
	}

	$scope.scoreBoard = function() {
		var param = {
			teamid: $scope.TEAM._id,
			matchid: $scope.matchid,
		}
		$http.get('/scoreboard/team/list?'+$.param(param)).success(function(datas){
			$scope.taskresult = datas.submit.tasks
			$scope.taskPass = datas.submit.solved
		})
	}
	
	// countdown match time
	$scope.NOTI   = true
	$scope.status = 'IDLE'
	$scope.mynotifications = {}
	$scope.notificationCount = 0
	$scope.timeMath = function() {
		$scope.onWait = function(){
			$http.get('/match/gettime?matchid=' + $scope.matchid).success(function(data) {
				$scope.status = data.status

				if($scope.status == 'IDLE'){
					console.log('wait for start...')
					$scope.NOTI   = false
					wait = $timeout($scope.onWait, 1000)
				} else {
					// init time
				    $scope.counter = parseInt(data.endseconds)
				    $scope.mytimes = jsTimeHHMMSS($scope.counter)
				    // time countdown interval
				    $scope.onInterval = function(){
				    	if($scope.counter <= 0 || $scope.status == 'IDLE') {
				    		console.log('stop interval')
				    		$interval.cancel($scope.myinterval)
				    		$interval.cancel($scope.myIntervalNotifiaction)
				    		wait = $timeout($scope.onWait, 1000)
				    	} else {
				    		$scope.counter--
				    		$scope.mytimes = jsTimeHHMMSS($scope.counter)
				    	}
				    }
				    $scope.myinterval = $interval($scope.onInterval, 1000)
					
					// notification interval
					$scope.NOTI   = true
					$scope.myIntervalNotifiaction = $interval(function(){
						$http.get('/notification/getcount?matchid=' + $scope.matchid).success(function(data){
							if(parseInt(data) != $scope.notificationCount){
								$scope.notificationCount = parseInt(data)
								$http.get('/notification/listall?matchid=' + $scope.matchid).success(function(datas){
									$scope.notificationDatas = datas
									if($scope.NOTI){
										$scope.showNotificationAlert()
									}
								})
							}
						})

						// realtime score
						$scope.scoreBoard()
						
						// console.log(Auth.isLoginTeam())
						// console.log(Auth.isLoginTeam()._id)
					}, 5000)
				}
			})
		}
		var wait = $timeout($scope.onWait, 1000)
	}

	// show notification 
	$scope.showNotificationAlert = function(){
		var notiNow = Notification({
			message: $scope.notificationDatas[0].Title, 
			title  : "ประกาศจาก Admin",
			replaceMessage: true
		})
		notiNow.then(function(notification){
			notification._templateElement.click(function(){
				$scope.showMessageDetail(0)
			})
		})
	}

	// show detail of notification
	$scope.showMessageDetail = function(index){
		$scope.mynotifications = {
			title  : $scope.notificationDatas[index].Title,
			message: $scope.notificationDatas[index].Message
		}						
		$('#myModal').modal('show')
	}

	// get time object {hh, mm, ss}
	function jsTimeHHMMSS(seconds) {
		if(seconds < 0) {
			return {
				h: "00",
				m: "00",
				s: "00"
			}
		}
		var times = {}

		times.h = Math.floor(seconds / (60 * 60))
    	times.m = Math.floor((seconds - (times.h * (60 * 60))) / 60)
    	times.s = seconds - (times.h * (60 * 60)) - (times.m * 60)

    	if (times.h < 10) { times.h = "0" + times.h }
    	if (times.m < 10) { times.m = "0" + times.m }
    	if (times.s < 10) { times.s = "0" + times.s }

    	return times
	}
	
	// get time end to seconds
	function jsTimeDiff(strDateTimeEnd){
		var start = new Date()
		var end = new Date(strDateTimeEnd)
		var totalTimes = (end.getTime() - start.getTime()) / (1000 * 60)
		
		return Math.round(totalTimes * 60);
	}
})