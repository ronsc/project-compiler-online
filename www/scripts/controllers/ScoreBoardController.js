app.controller('ScoreBoardController', function($scope, $http, $timeout, $interval, $routeParams) {
    $scope.chs = ['A', 'B', 'C', 'D', 'E', 'F']

    $scope.matchId = $routeParams.matchid

    // init team
    $scope.init = function(){
        $scope.lastUpdate = new Date()
        $interval(function(){
            $scope.lastUpdate = new Date()

            $http.get('/scoreboard/listall?matchid='+$scope.matchId).success(function(datas){
                $scope.datas = datas
            })
        }, 5000)

        $http.get('/scoreboard/listall?matchid='+$scope.matchId).success(function(datas){
            $scope.datas = datas
        })

        $http.get('/team/listall').success(function(teams){
            $scope.teams = teams
        })

        var param = {
            mode: "MATCH",
            matchid: $scope.matchId,
        }
        $http.get('/task/list/mode?'+$.param(param)).success(function(tasks){
            $scope.tasks = tasks
        })
    }

    // set class for status tasksubmit
    $scope.getClass = function(val, first){
        var result = "score-result"

        if(val.length == 0){
            // not submit
            result = "score-result"
        } else if(first){
            // submit first team
            result = "score-result-yellow"
        } else if(val[val.length-1] != 0){
            // submit pass
            result = "score-result-green"
        } else if(val[val.length-1] == 0) {
            // submit notpass
            result = "score-result-red"
        }

        return result
    }

    // show different style for ranking IN_RANG
    var IN_RANG = 3
    $scope.getRankStyle = function(val){
        if(val > IN_RANG)
            return {'background-color': '#f0f0f0', 'color': 'black'}
        else
            return {}
    }
})