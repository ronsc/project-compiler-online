app.controller('TaskSubmitController', function($scope, $http, $routeParams, Auth, $location, $timeout, $filter, Notification) {

	this.params = $routeParams
	$scope.user = Auth.isLogin().username

	$scope.init = function() {
		$http.get('/tasksubmit/list/train').success(function(tasks) {
			$scope.tasks = tasks
		})
	}

	// init tasksubmit data in datatables
	$scope.showTable = function() {
		if($routeParams.Path == 'me' || $routeParams.Path.indexOf('mecode') != -1) {
			$scope.title = 'ของฉัน'
		} else {
			$scope.title = 'ทั้งหมด'
		}
		var param = $routeParams.Path + '&user=' + $scope.user

		$('#tasksubmit_table').dataTable({
			ajax: '/tasksubmit/list/train?By=' + param,
			columns: [
				{data: '_id', searchable: false, orderable: false,},
				{data: 'Date'},
				{data: 'UserName'},
				{data: 'Code'},
				{data: 'Lang'},
				{data: 'Result'}
			],
			createdRow: function ( row, data, index ) {
				$('td', row).eq(0).html(index+1)

				var origin_date = $('td', row).eq(1)
				var conv_date = $filter('date')(origin_date.text(), 'dd/MM/yyyy hh:mm:ss')
	            origin_date.html(conv_date)
	        },
	        order: [ 1, 'desc' ],
	        language: {
	        	url: '/scripts/json/lang_th.json'
	        },
	        lengthMenu: [[20, 50, 100, -1], [20, 50, 100, "ทั้งหมด"]]
		})
	}

	// switch lang code
	$scope.changeLang = function() {
		ace.edit("editor").getSession().setMode("ace/mode/" + $scope.submit.lang)
	}

	// init ace editor
	$scope.initEditor = function() {
		var editor = ace.edit("editor")
		editor.setTheme("ace/theme/terminal")
		editor.getSession().setMode("ace/mode/" + $scope.submit.lang)
		
		document.getElementById('editor').style.fontSize='18px'
		editor.setOptions({
			enableSnippets: true
		})
	}

	// run task
	$scope.taskRun = function() {
		var value = ace.edit("editor").getSession().getValue()
		$scope.HTML = 'Compiling...'
		$('#btn-run').attr("disabled", "disabled")

		if(angular.equals(value, "")){
			$scope.HTML = 'Source Code Not Found!'
			$('#btn-run').removeAttr("disabled")
			return
		} else {
			var pathParams = {
				input: $scope.submit.input,
				code: $scope.submit.code,
				lang: $scope.submit.lang,
				username: $scope.submit.username
			}
			var path = $.param(pathParams)
			$http.post('/task/run?'+path, value).success(function(data) {
				// show result from server
				$scope.HTML = 'Compilation successful, '
				$scope.HTML += $filter('date')(new Date(), 'MM/dd/yyyy HH:mm:ss', '+0700')
				$scope.HTML += '<br/>'+data
				$('#btn-run').removeAttr("disabled")
			})
		}
	}

	// submit task
	$scope.submit = {}
	$scope.submit.code = this.params.Code
	$scope.submit.lang = 'c_cpp'
	$scope.submit.username = Auth.isLogin().username
	$scope.submit.mode = $location.search().mode
	$scope.taskSubmit = function() {
		$("#dialog-confirm").dialog({
		    resizable: false,
		    modal: true,
		    width: '350px',
		    buttons: {
		       	"ยืนยัน": function() {
		       		$(this).dialog("close")

					$scope.submit.value = ace.edit("editor").getSession().getValue()
					if(angular.equals($scope.submit.value, "")){
						Notification.error({
							message: 'ไม่พบ Source Code!', 
							replaceMessage: true,
							delay: 3000,
						})
						$('#btn-run').removeAttr("disabled")
						return
					} else {
						if($scope.submit.mode == 'MATCH'){
							$scope.submit.username = Auth.isLoginTeam().teamname
						} else {
							$scope.submit.mode = 'TRAIN'
						}
			
						var param = {
							code: $scope.submit.code,
							lang: $scope.submit.lang,
							username: $scope.submit.username,
							mode: $scope.submit.mode,
							teamid: Auth.isLoginTeam()._id,
							matchid: parseInt($routeParams.matchid),
						}

						console.log(param)
						$http.post('/task/submit?'+$.param(param), $scope.submit.value).success(function (data) {
							Notification.success({
								message: 'ส่ง Code เรียบร้อย!', 
								replaceMessage: true
							})
						})
					}
		        },
		       	"ยกเลิก": function() {
		          	$(this).dialog("close")
		       	}
		    }
		})
	}

	// read form input file to ace editor
	readSingleFile = function() {
		var file = $('#file-input')[0].files[0]
		if(!file){
			return
		}

		var fr = new FileReader()
		fr.onload = function(){
			ace.edit("editor").setValue(fr.result)
		}
		fr.readAsText(file)
	}
})