app.controller('HomeCtrl', function($scope, $cookieStore, Auth, $filter, $http) {
	$scope.user = Auth.isLogin()
	// console.log($scope.user)

	// get ranking data
	$scope.initRanking = function() {
		$http.get('/ranking/listall').success(function(datas) {
			if(datas != null) {
				$scope.rankings = datas
			} else {
				$scope.rankings = []
			}
		})
	}

	// init tasksubmit data in datatables
	$(document).ready(function(){
		$('#tasksubmit_table').dataTable({
			ajax: '/tasksubmit/list/train',
			columns: [
				{data: '_id', searchable: false, orderable: false,},
				{data: 'Date'},
				{data: 'UserName'},
				{data: 'Code'},
				{data: 'Lang'},
				{data: 'Result'}
			],
			createdRow: function ( row, data, index ) {
				$('td', row).eq(0).html(index+1)

				var origin_date = $('td', row).eq(1)
				var conv_date = $filter('date')(origin_date.text(), 'dd/MM/yyyy HH:mm:ss')
	            origin_date.html(conv_date)
	        },
	        order: [ 1, 'desc' ],
	        language: {
	        	url: '/scripts/json/lang_th.json'
	        },
	        lengthMenu: [[20, 50, 100, -1], [20, 50, 100, "ทั้งหมด"]]
		})
	})

	// select class different progressbar
	$scope.progressbarProcess = function(percen){
		var progressClass
		if(percen <= 25){
			progressClass = 'progress-bar-danger'
		} else if(percen <= 50){
			progressClass = 'progress-bar-warning'
		} else if(percen <= 75){
			progressClass = 'progress-bar-success'
		} else {
			progressClass = 'progress-bar-primary'
		}

		return progressClass
	}
})