app.controller('MatchController', function($scope, $http, $timeout, $interval, Notification, $routeParams, $filter) {
    $scope.params = $routeParams

    $scope.matchsettings = {
        timestart: new Date(1970, 0, 1, new Date().getHours(), new Date().getMinutes(), 0),
        timehours: 3
    }
    $scope.chs = [
        'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 
        'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'
    ]
    $scope.lastUpdate = new Date()
    $interval(function(){
        $scope.lastUpdate = new Date()
    }, 1000)

    $scope.moment = moment

    // init get task for compute match
	$scope.init = function() {
        $(document).ready( function() {
            $('#message').ckeditor()
            $('#details').ckeditor()
            // $('#myModal').modal('show')
        })

        $http.get('/match/list?matchid=' + $routeParams.MatchId).success(function(match) {
            $scope.matchsettings = match
            // convert time (string -> date)
            $scope.matchsettings.regstart = moment(match.regstart, 'yyyy-MM-dd').toDate()
            $scope.matchsettings.regend = moment(match.regend, 'yyyy-MM-dd').toDate()
            $scope.matchsettings.datematch = moment(match.datematch, 'yyyy-MM-dd').toDate()
            $scope.matchsettings.timestart = moment(match.timestart, 'HH:mm:ss').toDate()
            
            $scope.listTeam()
        })

        // fetch notification all
        $scope.listNotification()
	}

    $scope.listTeam = function() {
        $http.get('/team/match/list?matchid=' + $routeParams.MatchId).success(function(team) {
            $scope.matchsettings.team = team
        })
    }

    $scope.listNotification = function() {
        $http.get('/notification/listall?matchid=' + $routeParams.MatchId).success(function(data){
            $scope.notificationDatas = data
        })
    }

    // $scope.status = 'IDLE'
    $scope.timeMatchStart = function(){
        $http.get('/match/gettime?matchid='+$routeParams.MatchId).success(function(data) {
            $scope.matchsettings.failsubmit = data.failsubmit
            $scope.status  = data.status
            if($scope.status == 'IDLE'){
                $scope.counter = data.startseconds
                $('#btnStart').removeAttr('disabled')
            } else if($scope.status == 'RUN'){
                $scope.counter = data.endseconds
                $('#btnStart').attr('disabled', 'disabled')
            }
            $scope.mytimes = jsTimeHHMMSS($scope.counter)
        })  
    }
    $scope.timeMatchStart()
    
    // countdown time
    $scope.onInterval = function() {
        $scope.counter--
        $scope.mytimes = jsTimeHHMMSS($scope.counter)

        if($scope.counter <= 0) {
            $scope.timeMatchStart()
        }
    }
    $scope.myinterval = $interval($scope.onInterval, 1000)
    
    // setup time to start
    $scope.MatchSetup = function(){
        // get time object {hh, mm, ss}
        $scope.counter = $scope.matchsettings.timehours * 60 * 60
        $scope.mytimes = jsTimeHHMMSS($scope.counter)
        
        var hours      = $scope.matchsettings.timestart.getHours()
        var minutes    = $scope.matchsettings.timestart.getMinutes()
        
        var timestart  = jsTimeHHMMSS((hours * 60 * 60) + (minutes * 60))
        var timeend    = jsTimeHHMMSS(((hours + $scope.matchsettings.timehours) * 60 * 60) + (minutes * 60))
        
        var time_start = $scope.matchsettings.timestart
        var time_hours = $scope.matchsettings.timehours
        var datas      = {
            matchid:    $routeParams.MatchId,
            timestart:  moment(time_start).format('HH:mm:ss'),
            timeend:    moment(time_start).add(time_hours, 'hours').format('HH:mm:ss'),
            timehours:  time_hours,
            failsubmit: $scope.matchsettings.failsubmit,
            details:    CKEDITOR.instances.details.getData(),
            status:     'IDLE',
            name:       $scope.matchsettings.name,
            regstart:   $scope.matchsettings.regstart,
            regend:     $scope.matchsettings.regend,
            datematch:  $scope.matchsettings.datematch,
            public:     $scope.matchsettings.public,
        }

        // save to db
        $http.post('/match/setup?matchid='+$routeParams.MatchId, datas).success(function(data) {
            $scope.timeMatchStart()

            Notification.success({
                message: 'อัพเดทข้อมูลสำเร็จ', 
                replaceMessage: true,
                delay: 5000
            })

            // hide settings
            $('#settings_match').toggleClass('hide')
        })
    }

    // start match this now
    $scope.MatchStart = function() {
        // get time object {hh, mm, ss}
        $scope.counter = $scope.matchsettings.timehours * 60 * 60
        $scope.mytimes = jsTimeHHMMSS($scope.counter)
        
        var date       = new Date()
        var hours      = date.getHours()
        var minutes    = date.getMinutes()
        var seconds    = date.getSeconds()
        
        var timestart  = jsTimeHHMMSS((hours * 60 * 60) + (minutes * 60) + seconds)
        var timeend    = jsTimeHHMMSS(((hours + $scope.matchsettings.timehours) * 60 * 60) + (minutes * 60))
        
        var datas      = {
            matchid   : $routeParams.MatchId,
            timestart : timestart.h+':'+timestart.m+':'+timestart.s,
            timeend   : timeend.h+':'+timeend.m+':'+timeend.s,
            timehours : $scope.matchsettings.timehours,
            failsubmit: $scope.matchsettings.failsubmit,
            status    : 'RUN'
        }

        $http.post('/match/setup?matchid='+$routeParams.MatchId, datas).success(function(data) {
            $scope.timeMatchStart()
        })
    }

    $scope.MatchReset = function() {
        $http.get('/match/list?matchid=' + $routeParams.MatchId).success(function(match) {
            $scope.notification = match
            // set value on details (ckeditor)
            CKEDITOR.instances.details.setData(match.details)

            $scope.timeMatchStart()

            // hide settings
            $('#settings_match').toggleClass('hide')
        })
    }
    
    // send message to client
    $scope.notification = {
        title: "",
        message: ""
    }
    $scope.MatchSendMessage = function(){           
        $scope.notification.message = CKEDITOR.instances.message.getData()
        var data = $scope.notification

        $scope.errMsg = ''
        if(data.title == ""){
            $('#title').focus()
            $scope.errMsg = '* กรุณาใส่หัวข้อ'
            return
        }else if(data.message == ""){
            $('#message').focus()
            $scope.errMsg = '* กรุณาใส่ข้อความ'
            return
        }

        $('#myModal').modal('hide')
        $http.post('/notification/save?matchid='+$routeParams.MatchId, data).success(function(data){
            $scope.notification.title = ''
            $scope.notification.message = ''
            
            // reload list notification
            $scope.listNotification()
        })
    }

    // view all submit form team
    $scope.DATA = {}
    $scope.matchViewSubmit = function(code, type ,title){
        var status = 'send'
        if(type == 'send'){
            title = '[ส่ง] ' + title
        } else {
            title = '[ผ่าน] ' + title
            status = 'pass'
        }

        var param = {
            code: code,
            status: status,
            matchid: $routeParams.MatchId,
        }
        $http.get('/tasksubmit/list/match/code?' + $.param(param)).success(function(data){
            $scope.DATA = {
                title: title,
                content: data
            }
            $('#view_modal').modal('show')
        })
    }

    // generate random password for team
    $scope.generatePassword = function() {
        var length   = 8
        var charset  = "abcdefghijklnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        var password = []
        var pwTmp

        var n = charset.length
        for(var j = 0; j < $scope.matchsettings.team.length; j++) {
            pwTmp = ""
            for(var i = 0; i < length; i++) {
                var x = Math.floor(Math.random() * n)
                pwTmp += charset.charAt(x)
            }
            password[j] = {
                teamname: $scope.matchsettings.team[j].teamname, 
                password: pwTmp
            }   
        }
        $http.post('/team/generatepassword?matchid=' + $routeParams.MatchId, password).success(function(data) {
            Notification.success({
                message: 'สร้างรหัสผ่าน สำเร็จ', 
                replaceMessage: true,
                delay: 3000
            })

            // reload list team
            $scope.listTeam()
        })
    }

    // get time object {hh, mm, ss}
    function jsTimeHHMMSS(seconds) {
        var times = {}

        times.h = Math.floor(seconds / (60 * 60))
        times.m = Math.floor((seconds - (times.h * (60 * 60))) / 60)
        times.s = seconds - (times.h * (60 * 60)) - (times.m * 60)

        if (times.h < 10) { times.h = "0" + times.h }
        if (times.m < 10) { times.m = "0" + times.m }
        if (times.s < 10) { times.s = "0" + times.s }

        return times
    }

    // get time object {hh, mm, ss}
    function jsTimeHHMMSS(seconds) {
        var times = {}

        times.h = Math.floor(seconds / (60 * 60))
        times.m = Math.floor((seconds - (times.h * (60 * 60))) / 60)
        times.s = seconds - (times.h * (60 * 60)) - (times.m * 60)

        if (times.h < 10) { times.h = "0" + times.h }
        if (times.m < 10) { times.m = "0" + times.m }
        if (times.s < 10) { times.s = "0" + times.s }

        return times
    }
})