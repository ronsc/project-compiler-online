app.controller('AdminController', function(
	$scope, $http, Notification, $filter, $routeParams, $window, $route) {

	$scope.params = $routeParams
	console.log($scope.params)
	
	// check update
	$scope.UPDATE = ($routeParams.process == "update")

	$scope.init = function() {
		if($scope.UPDATE) {
			$('input:radio').attr('disabled', 'disabled')
			$('input:file').hide()
			$('form[name="frm_task"]').attr('novalidate', 'novalidate')

			var code = $routeParams.code
			$http.get('/task/list/code/edit?code='+code).success(function(data) {
				$scope.task = data.data
				$scope.input = data.input
				$scope.output =  data.output

				$('textarea#content').ckeditor()
				$('textarea#help').ckeditor()
			})
		} else {
			$('textarea#content').ckeditor()
			$('textarea#help').ckeditor()
			// init new code
			$scope.getCode()
		}
	}

	/* --- Manage Team --- */
	// fetch team data for generate password
	$scope.listTeam = function() {
		$http.get('/team/listall').success(function(data) {
			$scope.datas = data
		})
	}
	// generate random password for team
	$scope.generatePassword = function() {
		var length   = 8
		var charset  = "abcdefghijklnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
		var	password = []
		var pwTmp

		var n = charset.length
		for(var j = 0; j < $scope.datas.length; j++) {
			pwTmp = ""
			for(var i = 0; i < length; i++) {
				var x = Math.floor(Math.random() * n)
				pwTmp += charset.charAt(x)
			}
			password[j] = {
				teamname	: $scope.datas[j].teamname, 
				password 	: pwTmp
			}	
		}
		$http.post('/team/generatepassword', password).success(function(data) {
			Notification.success({
				message: 'สร้างรหัสผ่าน สำเร็จ', 
				replaceMessage: true,
				delay: 3000
			})

			// reload list team
			$scope.listTeam()
		})
		// $scope.save_show = true
	}

	/* --- Manage Task --- */
	$scope.task = {}
	$scope.task.level = "EASY"
	$scope.task.mode = "TRAIN"
	if($scope.params.Mode == "match") {
		$scope.task.mode = "MATCH"
		$scope.task.level = "HARD"
	}
	$scope.save = function() {		
		if(!$scope.UPDATE) {
			if(!$scope.frm_task.title.$valid) {
				Notification.warning({
					message: 'กรุณาใส่ ชื่อปัญหา', 
					replaceMessage: true,
					delay: 5000
				})
				return
			} else if($('#inputFile').val() == "") {
				Notification.warning({
					message: 'กรุณาเลือก File Input', 
					replaceMessage: true,
					delay: 5000
				})
				return
			} else if($('#outputFile').val() == "") {
				Notification.warning({
					message: 'กรุณาเลือก File Output', 
					replaceMessage: true,
					delay: 5000
				})
				return
			} else {
				var files = {
					"inputFile" 	: document.getElementById('inputFile').files[0],
					"outputFile" 	: document.getElementById('outputFile').files[0]
				}
		        $scope.task.content = CKEDITOR.instances.content.getData()
				$scope.task.help = CKEDITOR.instances.help.getData()

		        var fd = new FormData()
		        // append task data
		        angular.forEach($scope.task, function(value, key) {
	            	fd.append(key, value)
		        })
		        // append file
		        angular.forEach(files, function(value, key) {
		            fd.append(key, value)
		        })

		        var params = {
		        	user: 'admin',
		        	mode: $scope.task.mode,
		        	matchid: $routeParams.MatchId
		        }
		    	
		        $http.post('/task/uploadio?' + $.param(params), fd, {
		            transformRequest: angular.identity,
		            headers: {
		            	'Content-Type': undefined
		            }
		        }).success(function(){
			        Notification.success({
						message: 'บันทึกโจทย์สำเร็จ', 
						replaceMessage: true,
						delay: 3000
					})

			        // clear data
			        $scope.task = {}
			        $scope.task.mode = "TRAIN"
			        $scope.task.level = "EASY"
			        // clear file input
			        var $inputFile = $('#inputFile')
			        $inputFile.replaceWith($inputFile.clone())
			        var $outputFile = $('#outputFile')
			        $outputFile.replaceWith($outputFile.clone());
			        
			        $window.location.reload()
		        })
			}
		} else {
			$scope.task.content = CKEDITOR.instances.content.getData()
			$scope.task.help = CKEDITOR.instances.help.getData()

			$scope.task.input = $('#f_input').val()
			$scope.task.output = $('#f_output').val()

			$http.post('/task/update', $scope.task).success(function(data) {
				Notification.success({
					message: 'อัพเดทข้อมูล สำเร็จ<br />' + data.message, 
					replaceMessage: true,
					delay: 3000
				})
			})
		}
	}
	// recieve code for create task
	$scope.getCode = function() {
		var params = {
			mode: $scope.task.mode,
			level: $scope.task.level,
			matchid: $routeParams.MatchId
		}
		$http.get('/task/getnewcode?' + $.param(params)).success(function(data) {
			$scope.task.code = data
		})
	}
	// preview task before save to database
	$scope.Task_preview = function() {
		$scope.task.content = CKEDITOR.instances.content.getData()
		$scope.task.help = CKEDITOR.instances.help.getData()

		$('#myModal').modal('show')
	}

	// list task data to datatable
	$scope.Task_List = function() {
		$scope.$table = $('#tasksubmit_table').dataTable({
			ajax: '/task/listall',
			columns: [
				{data: null, searchable: false, orderable: false},
				{data: 'code'},
				{data: 'title'},
				{data: 'sendtotal', searchable: false},
				{data: 'passtotal', searchable: false},
				{data: null},
				{data: 'date'},
				{data: null, searchable: false, orderable: false},
			],
			createdRow: function ( row, data, index ) {
				$('td', row).eq(0).html((index+1)+'.')
				$('td', row).eq(5).html('admin')
				var conv_date = $filter('date')(data.date, 'dd/MM/yyyy HH:mm:ss')
	            $('td', row).eq(6).html(conv_date)
	            var btn_edit = '<button class="btn btn-warning btn-xs">'
	            btn_edit += '<i class="fa fa-edit"></i> แก้ไข</button>'
	            var btn_delete = '<a class="btn btn-danger btn-xs">'
	            btn_delete += '<i class="fa fa-trash"></i> ลบ</a>'
	            $('td', row).eq(7).html(btn_edit+'&nbsp;'+btn_delete)
	        },
	        order: [ 0, 'desc' ],
	        language: {
	        	url: '/scripts/json/lang_th.json'
	        },
	        lengthMenu: [[30, 50, 100, -1], [30, 50, 100, "ทั้งหมด"]],
		})
		
		// onclick edit button
		$('#tasksubmit_table tbody').on('click', '.btn-warning', function () {
	        var code = $(this).parents('tr').find('td').eq(1).text()
	        $window.location.href = '#/admin/manage/task/create/train?process=update&code='+code
	    });
	    // onclick delete button
		$('#tasksubmit_table tbody').on('click', '.btn-danger', function () {
	        var code = $(this).parents('tr').find('td').eq(1).text()
	        $("#dialog-confirm").dialog({
		      	resizable: false,
		      	modal: true,
		      	width: '350px',
		      	buttons: {
		        	"ยืนยัน": function() {
		        		$scope.Taks_Delete(code)
		          		$(this).dialog("close")
		        	},
		        	"ยกเลิก": function() {
		          		$(this).dialog("close")
		        	}
		      	}
		    })
		})
	}
	// delete task from db
	$scope.Taks_Delete = function(code) {
		$http.get('/task/delete?code='+code).success(function(data) {
			if(data.status == "success") {
				Notification.success({
					message: 'ลบโจทย์ '+data.code+' สำเร็จ', 
					replaceMessage: true,
					delay: 3000
				})
				// reload data
				$scope.$table._fnAjaxUpdate()
			} else {
				Notification.warning({
					message: 'ลบโจทย์ '+data.code+' ไม่สำเร็จ<br />' + data.message, 
					replaceMessage: true,
					delay: 3000
				})
			}
		})
	}

	/* Manage Member */
	// datatable member list
	$scope.reg_status = {
		"general": "บุคคลทั่วไป",
		"teacher": "ครู / อาจารย์",
		"student": "นักเรียน / นักศึกษา"
	}
	$scope.Member_List = function() {
		// {
		// 	"_id":"5545c9fd96820d929e7ba717",
		// 	"address":"",
		// 	"birthday":"2536-05-13T00:00:00+07:00",
		// 	"codename":"ADMIN",
		// 	"date":"0001-01-01T00:00:00Z",
		// 	"email":"fdroncpe17@gmail.com",
		// 	"facebook":"",
		// 	"lastname":"จันทร์นาลาว",
		// 	"level":"ADMIN",
		// 	"name":"รณกร",
		// 	"password":"1",
		// 	"province":"กรุงเทพมหานคร",
		// 	"sex":"M",
		// 	"status":"general",
		// 	"tel":"",
		// 	"username":"admin",
		// 	"website":""
		// }
		$scope.$table = $('#member_table').dataTable({
			ajax: '/member/listall',
			columns: [
				{data: null, searchable: false, orderable: false},
				{data: 'codename'},
				{data: 'username'},
				{data: 'name'},
				// {data: 'lastname'},
				{data: 'sex'},
				// {data: 'email'},
				{data: 'date'},
				{data: 'level'},
				{data: null, searchable: false, orderable: false},
			],
			createdRow: function ( row, data, index ) {
				$('td', row).eq(0).html((index+1)+'.')

				$('td', row).eq(3).html(data.name+' '+data.lastname)

				$('td', row).eq(4).html(data.sex=='M'?'ชาย':'หญิง')

				var conv_date = $filter('date')(data.date, 'dd/MM/yyyy HH:mm:ss')
	            $('td', row).eq(5).html(conv_date)

	           	var btn_view = '<button class="btn btn-success btn-xs">'
	            btn_view += '<i class="fa fa-eye"></i></button>'

	            var btn_switch = '<button class="btn btn-primary btn-xs" title="เปลี่ยนระดับ">'
	            btn_switch += '<i class="fa fa-exchange"></i></button>'

	            var btn_delete = '<a class="btn btn-danger btn-xs">'
	            btn_delete += '<i class="fa fa-trash"></i></a>'

	            $('td', row).eq(7).html(btn_switch+'&nbsp;'+btn_view+'&nbsp;'+btn_delete)
	        },
	        order: [ 0, 'DESC' ],
	        language: {
	        	url: '/scripts/json/lang_th.json'
	        },
	        lengthMenu: [[30, 50, 100, -1], [30, 50, 100, "ทั้งหมด"]],
		})
		
		// onclick view button
		$('#member_table tbody').on('click', '.btn-primary', function () {
	        var user = $(this).parents('tr').find('td').eq(2).text()

			$("#dialog-confirm").dialog({
				title: 'ยืนยันการเปลี่ยนแปลงระดับผู้ใช้งาน',
		      	resizable: false,
		      	modal: true,
		      	width: '350px',
		      	buttons: {
		        	"ยืนยัน": function() {
		        		$scope.Member_ChangeLevel(user)
		          		$(this).dialog("close")
		        	},
		        	"ยกเลิก": function() {
		          		$(this).dialog("close")
		        	}
		      	}
		    })
	    })
		// onclick view button
		$('#member_table tbody').on('click', '.btn-success', function () {
	        var user = $(this).parents('tr').find('td').eq(2).text()
	        
	        $http.get('/member/finduser?us='+user).success(function(data){
	        	$scope.userdetail = data
		        $('#myModal').modal('show')
			})
	    })
	    // onclick delete button
		$('#member_table tbody').on('click', '.btn-danger', function () {
	        var user = $(this).parents('tr').find('td').eq(2).text()
	        $("#dialog-confirm").dialog({
	        	title: 'คุณต้องการที่จะลบผู้ใช้งานนี้หรือไม่?',
		      	resizable: false,
		      	modal: true,
		      	width: '350px',
		      	buttons: {
		        	"ยืนยัน": function() {
		        		$scope.Member_Delete(user)
		          		$(this).dialog("close")
		        	},
		        	"ยกเลิก": function() {
		          		$(this).dialog("close")
		        	}
		      	}
		    })
		})
	}

	// delete member from db
	$scope.Member_Delete = function(user) {
		$http.get('/member/delete?user='+user).success(function(data) {
			if(data.status) {
				Notification.success({
					message: data.message, 
					replaceMessage: true,
					delay: 3000
				})
				// reload data
				$scope.$table._fnAjaxUpdate()
			} else {
				Notification.error({
					message: data.message, 
					replaceMessage: true,
					delay: 5000
				})
			}
		})
	}
	// Change Level member from db
	$scope.Member_ChangeLevel = function(user) {
		$http.get('/member/changelevel?user='+user).success(function(data) {
			if(data.status) {
				Notification.success({
					message: data.message, 
					replaceMessage: true,
					delay: 3000
				})
				// reload data
				$scope.$table._fnAjaxUpdate()
			} else {
				Notification.error({
					message: data.message, 
					replaceMessage: true,
					delay: 5000
				})
			}
		})
	}
})