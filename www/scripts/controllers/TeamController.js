app.controller('TeamController', function($scope, $http, $location, Member, 
	$timeout, $interval, Notification, Auth, $routeParams) {
	
	$scope.user = Auth.isLogin()
	$scope.chs = ['A', 'B', 'C', 'D', 'E', 'F']

	$scope.MatchId = $routeParams.MatchId
	$scope.team = {}

	// init for team regis
	$scope.init = function() {
		Member.initUserName($scope.user.username)
		var usernames = Member.getUserName()

		// member 
		$('#username').typeahead({
		  	hint: true,
		  	highlight: true,
		  	minLength: 1
		},{
			source: substringMatcher(usernames)
		})

		$('#username').bind('typeahead:select', function(ev, suggestion) {
			$http.get('/member/finduser?us=' + suggestion).success(function(member) {
				if($scope.member_select == 1) {
					Member.initUserName(suggestion)
					$('#username').typeahead('destroy');
					$('#username').typeahead({
					  	hint: true,
					  	highlight: true,
					  	minLength: 1
					},{
						source: substringMatcher(Member.getUserName())
					})

					$scope.team.member2 = member

					$('#username').val('')
				} else {
					$scope.team.member3 = member
					$('#username').val('')
				}
			})		  	
		}) 

		$scope.checkTeam()
	}

	$scope.checkTeam = function() {
		var param = {
			user: $scope.user.username,
			matchid: parseInt($scope.MatchId),
		}
		$http.get('/team/checkreg?' + $.param(param)).success(function(team) {
			$scope.myteam = team

			if(team.status) {
				if(team.head) {
					$scope.team.member1 = Auth.isLogin()
				} else {
					$scope.team.member1 = team.members[0]
				}
				$scope.team.member2 = team.members[1]
				$scope.team.member3 = team.members[2]

				if($scope.myteam.data.member1 == $scope.user.username) {
					$('tbody tr').eq(0).addClass('bg-primary')

				} else if($scope.myteam.data.member2 == $scope.user.username) {
					$('tbody tr').eq(1).addClass('bg-primary')

					if(!$scope.myteam.data.member2status) {
						var html = '<span class="fa fa-times"></span>&nbsp;'
						html += 'คุณยังไม่ได้ยอมรับการเข้าร่วมทีม'
						$('h3.text-success').attr('class', 'text-warning').html(html)
					} else {
						var html = '<span class="fa fa-check"></span>&nbsp;'
						html += 'คุณได้ทำการสมัครแข่งขันเรียบร้อย'
						$('h3.text-warning').attr('class', 'text-success').html(html)
					}
				} else if($scope.myteam.data.member3 == $scope.user.username) {
					$('tbody tr').eq(2).addClass('bg-primary')

					if(!$scope.myteam.data.member3status) {
						var html = '<span class="fa fa-times"></span>&nbsp;'
						html += 'คุณยังไม่ได้ยอมรับการเข้าร่วมทีม'
						$('h3.text-success').attr('class', 'text-warning').html(html)
					} else {
						var html = '<span class="fa fa-check"></span>&nbsp;'
						html += 'คุณได้ทำการสมัครแข่งขันเรียบร้อย'
						$('h3.text-warning').attr('class', 'text-success').html(html)
					}
				}
			} else {
				$scope.team.member1 = Auth.isLogin()
			}
		})
	}

	// save team member to db
	$scope.save = function() {
		if($scope.teamname === undefined){
			Notification.error({
				message: 'กรุณากรอกชื่อทีม', 
				replaceMessage: true,
				delay: 5000
			})
		} else if($scope.team.member2 === undefined || $scope.team.member3 === undefined) {
			Notification.error({
				message: 'กรุณาเลือกสมาชิกให้ครบ', 
				replaceMessage: true,
				delay: 5000
			})
		} else {
			var team = {
				teamname : $scope.teamname,
				member1 : $scope.team.member1.username,
				member2 : $scope.team.member2.username,
				member3 : $scope.team.member3.username,
				matchid : parseInt($scope.MatchId),
			}

			console.log(team)

			$http.post('/team/registration', team).success(function(data) {
				if(data.status) {
					Notification.success({
						message: data.message, 
						replaceMessage: true
					})
					
					$timeout(function() {
						document.location.reload(true)
					}, 1000)
				} else {
					Notification.error({
						message: data.message, 
						replaceMessage: true
					})
				}
			})
		}
	}

	$scope.selectMember = function(index) {
		$scope.member_select = index
		$('#selectMemberModal').modal('show')
	}

	$scope.acceptTeam = function(member) {
		var data = {
			member: member,
			user: $scope.user.username,
			teamid: $scope.myteam.data._id
		}

		console.log(data)

		$http.get('/team/acceptmember?'+$.param(data)).success(function(data) {
			if(data.status) {
				Notification.success({
					message: data.message, 
					replaceMessage: true,
					delay: 5000
				})

				$scope.checkTeam()
				
				// $timeout(function() {
				// 	document.location.reload(true)
				// }, 2000)
			}
		})
	}

	$scope.MatchPublic = function() {
		$http.get('/match/public').success(function(data) {
			$scope.match_public = data
		})
	}

	// modal show match details
	$scope.showDetails = function(index) {
		$scope.showDetailsData = $scope.match_public[index].details

		$('#myModal').modal('show');
	}

	$scope.matchList = function() {
		$http.get('/match/list?matchid=' + $scope.MatchId).success(function(data) {
			$scope.match = data
		})
	}

})