app.factory('Auth', function($cookieStore) {
	var user = false
	var team = false
	var userCookies = $cookieStore.get('Logged')
	var teamCookies = $cookieStore.get('LoggedTeam')

	return {
		setUser : function(newUser) {
			user = newUser
			$cookieStore.remove('Logged')
			$cookieStore.put('Logged', newUser)
		},
		isLogin : function() {
			return (angular.isDefined(userCookies)) ? userCookies : false
		},
		setTeam : function(newTeam){
			team = newTeam
			$cookieStore.remove('LoggedTeam')
			$cookieStore.put('LoggedTeam', newTeam)
		},
		isLoginTeam : function() {
			return (angular.isDefined(teamCookies)) ? teamCookies : false
		}
	}
})

app.run(function($rootScope, $location, $route, Auth, $cookieStore) {
	$rootScope.$on('$routeChangeStart', function(event) {
		var user = Auth.isLogin()
		var team = Auth.isLoginTeam()
		
		if ($location.path() == '/logout') {
			$cookieStore.remove('Logged')
			$cookieStore.remove('LoggedTeam')
			$location.path('/')
			window.location.reload()
		}

		// if (!user) {
		// 	$location.path('/login')
		// } else {
		// 	$location.path($location.path())
		// }
		// console.log('Logged : ' + $cookieStore.get('Logged'))
		
		// || $location.path() == '/team/login'
		if($location.path() == '/team/match' || $location.path() == '/team/login'){
			// || user.level == 'ADMIN'
			if(team) {
				// console.log('200 login')
				$location.path('/team/match')
			} else {
				// console.log('pls login')
				$location.path('/team/login')
			}
		} 
	})
})

app.controller('LoginController', function($scope, $http, Auth, $location, Notification) {
	$scope.user = Auth.isLogin() || Auth.isLoginTeam()
	
	$scope.initLoginTeam = function() {
		$http.get('/team/match/listall').success(function(data){
			$scope.matchLoginData = data
		})
	}

	$scope.Login = function() {
		var us = $scope.userName
		var pw = $scope.passWd

		$http.get('/member/checklogin?' + $.param({us: us, pw: pw}))
		.success(function(user) {
			Auth.setUser(user)
			if (angular.isObject(user)) {
				if (user.level == 'ADMIN')
					$location.path('/')
				else
					$location.path('/')
				window.location.reload()
			} else {
				Notification.error({
					message: 'ชื่อผู้ใช้ / รหัสผ่าน ไม่ถูกต้อง',
					replaceMessage: true
				})
			}
		})
	}

	$scope.LoginTeam = function() {
		if($scope.login.match == undefined) {
			Notification.error({
				message: 'กรุณาเลือกแมทซ์',
				replaceMessage: true,
				delay: 3000,
			})
			return
		} else if($scope.login.team == undefined) {
			Notification.error({
				message: 'กรุณาเลือกทีม',
				replaceMessage: true,
				delay: 3000,
			})
			return
		}

		var param = {
			id: $scope.login.team, 
			pw: $scope.login.password
		}
		$http.get('/team/checklogin?' + $.param(param)).success(function(team) {
			Auth.setTeam(team)
			if(angular.isObject(team)) {
				$location.path('/team/match')
				window.location.reload()
			} else {
				Notification.error({
					message: 'รหัสผ่าน ไม่ถูกต้อง',
					replaceMessage: true,
					delay: 3000,
				})
			}
		})
	}

	$scope.getTeam = function() {
		$http.get('/team/match/listall?matchid=' + $scope.login.match)
		.success(function(data){
			$scope.matchLoginData = data
		})
	}
})