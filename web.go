package main

import (
	"fmt"
	"log"
	"net/http"
	// "os"

	_ "go-code/project-compiler-online/src/routes"
	// _ "./src/routes"
	// _ "cplonline/src/routes"
)

func main() {
	// auto init routes
	fs := http.FileServer(http.Dir("www"))
	http.Handle("/", fs)

	bind := fmt.Sprintf(":8080")
	// bind := fmt.Sprintf("%s:%s", os.Getenv("HOST"), os.Getenv("PORT")
	log.Println("running server on", bind)
	err := http.ListenAndServe(bind, nil)

	if err != nil {
		panic(err)
	}
}
